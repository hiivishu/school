package app.com.shiksha.adapter;

import android.app.DownloadManager;
import android.content.Context;

import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import app.com.shiksha.R;
import app.com.shiksha.model.HomeWork;


/**
 * Created by vishal on 12/8/16.
 */

public class HomeWorkAdapter extends RecyclerView.Adapter<HomeWorkAdapter.ViewHolder> {

    public ArrayList<HomeWork> homework_data;


    private Context context;


    public HomeWorkAdapter(Context applicationContext, ArrayList<HomeWork> homework_data) {
        this.context = applicationContext;
        this.homework_data = homework_data;
    }

    @Override
    public HomeWorkAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.homework_lay,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HomeWorkAdapter.ViewHolder holder, int position) {
        final HomeWork homeWork = getItem(position);
        holder.homeworkdate1.setText(homeWork.getTitle());
        holder.pact.setText(homeWork.getSubject());
        holder.pact2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = homeWork.getHomework_status();
                //download file
                Toast.makeText(context, "Downloading started", Toast.LENGTH_SHORT).show();
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                request.setDescription("Downloading HomeWork");
                request.setTitle("HomeWork");
                // in order for this if to run, you must use the android 3.2 to compile your app
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    request.allowScanningByMediaScanner();
                    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                }
                request.setDestinationInExternalPublicDir("Shiksha/", homeWork.getTitle() );

                // get download service and enqueue file
                DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                manager.enqueue(request);
                Toast.makeText(context, "Downloading started", Toast.LENGTH_LONG).show();
            }
        });



    }

    @Override
    public int getItemCount() {
        return homework_data.size();

    }

    private HomeWork getItem(int position) {
        return position < homework_data.size() ? homework_data.get(position) : null;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        HomeWork homeWork;
        TextView homeworkdate,homeworkdate1,pact,pact2;
        RecyclerView recyclerView;
        RelativeLayout relativeLayout;
        public ViewHolder(final View view) {
            super(view);

            relativeLayout = (RelativeLayout)view.findViewById(R.id.sub_details);
            homeworkdate = (TextView)view.findViewById(R.id.homeworkdate);
            homeworkdate1 = (TextView)view.findViewById(R.id.subject_name_homework21);
            recyclerView = (RecyclerView)view.findViewById(R.id.subject_details);
            pact = (TextView)view.findViewById(R.id.homework_status21);
            pact2 = (TextView)view.findViewById(R.id.view_details21);


            Typeface font5 = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            homeworkdate.setTypeface(font5);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            homeworkdate1.setTypeface(font);
            Typeface font1 = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            pact.setTypeface(font1);
            Typeface font2 = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            pact2.setTypeface(font2);


        }

    }
}