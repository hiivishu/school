package app.com.shiksha.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.helper.ChatMessage;

/**
 * Created by vishal on 11/23/16.
 */

public class ChatArrayAdapter extends ArrayAdapter<ChatMessage> implements ListAdapter {


    private TextView chatText;
    private List<ChatMessage> chatMessageList = new ArrayList<>();
    private Context context;


    public ChatArrayAdapter(Context context, int textViewResourseId) {

        super(context, textViewResourseId);
        this.context = context;
    }

    public void add(ChatMessage object) {
        chatMessageList.add(object);
        super.add(object);
    }

    public int getCount() {
        return this.chatMessageList.size();
    }

    public ChatMessage getItem(int index) {
        return this.chatMessageList.get(index);
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        ChatMessage chatMessageObj = getItem(position);
        View row = convertView;
        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        row = inflater.inflate(R.layout.right, parent, false);

        chatText = (TextView) row.findViewById(R.id.msgr);
        chatText.setText(chatMessageObj.message);
        return row;
    }
}
