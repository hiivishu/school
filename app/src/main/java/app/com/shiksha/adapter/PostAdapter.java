package app.com.shiksha.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.squareup.picasso.Picasso;

import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.activity.CommentActivity;
import app.com.shiksha.helper.UrlSupport;
import app.com.shiksha.model.Post;
import at.blogc.android.views.ExpandableTextView;

/**
 * Created by vishal on 11/11/16.
 */

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder> {
    public List<Post> post_data;
    SharedPreferences sharedPreferences = null;
    SharedPreferences.Editor editor;
    int user_id;
    String gurdian_id;
    private Context context;


    public PostAdapter(Context context, List<Post> post_data) {
        this.post_data = post_data;
        this.context = context;
    }

    @Override
    public PostAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_lay, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PostAdapter.ViewHolder holder, int position) {

        Post post = post_data.get(position);
        holder.post_details.setText(post.getPost());
        holder.post_heading.setText(post.getTitle());
        holder.date_time.setText(post.getCreated_at());
        holder.comment_count.setText(Integer.toString(post.getComment()));
        Picasso.with(context)
                .load(post.getPost_image_url())
                .placeholder(R.color.simplegray) //this is optional the image to display while the url image is downloading
                .into(holder.post_img);
        holder.post_details.setText(post.getPost());
        if (post.getPost().length()>=30){
            holder.read_more.setVisibility(View.VISIBLE);
        }else {
            holder.read_more.setVisibility(View.GONE);
        }
        holder.share_count.setText(Integer.toString(post.getShare()));
        holder.like_count.setText(Integer.toString(post.getLike()));
        if (post.getIs_liked().equals("true")) {
            holder.liked_img.setVisibility(View.VISIBLE);
        } else if (post.getIs_liked().equals("false")) {
            holder.like_img.setVisibility(View.VISIBLE);
        }

//

    }

    @Override
    public int getItemCount() {
        return post_data.size();
    }


    /*Like API STart..................................................................*/
    private void likeapi(int id) {
        sharedPreferences = context.getSharedPreferences("Login_pref", 0);
        editor = sharedPreferences.edit();
        user_id = sharedPreferences.getInt("id", 0);
        gurdian_id = String.valueOf(user_id);
        StringRequest stringrequest = new StringRequest(Request.Method.GET, UrlSupport.Like + "post_id=" + id + "&guardian_id=" + gurdian_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        Log.e("like url with", UrlSupport.Like + "post_id=" + id + "&guardian_id=" + gurdian_id);
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringrequest);

    }

    /*Like Api END........................................................................*/

    /*Share Api Start.....................................................................*/

    private void shareapi(int id) {
        sharedPreferences = context.getSharedPreferences("Login_pref", 0);
        editor = sharedPreferences.edit();
        user_id = sharedPreferences.getInt("id", 0);
        gurdian_id = String.valueOf(user_id);

        StringRequest stringrequest = new StringRequest(Request.Method.GET, UrlSupport.Share + "post_id=" + id + "&guardian_id=" + gurdian_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        Log.e("share url with", UrlSupport.Share + "post_id=" + id + "&guardian_id=" + gurdian_id);
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringrequest);


    }

    /*Share Api End...........................................................................*/



    /*Post ViewHolderCLass...............................................*/

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView post_heading, like_count, share_count, comment_count, date_time, comment_text, read_more;
        ImageView like_img, comment_img, share_img, post_img, liked_img;
        ExpandableTextView post_details;
        WebView webView;

        Post post;

        public ViewHolder(final View view) {
            super(view);

            post_heading = (TextView) view.findViewById(R.id.name);
            date_time = (TextView) view.findViewById(R.id.date_time);
            post_details = (ExpandableTextView) view.findViewById(R.id.post_details);
            like_count = (TextView) view.findViewById(R.id.like_count);
            share_count = (TextView) view.findViewById(R.id.share_count);
            comment_count = (TextView) view.findViewById(R.id.comment_count);
            like_img = (ImageView) view.findViewById(R.id.like_image);
            comment_img = (ImageView) view.findViewById(R.id.comment_img);
            share_img = (ImageView) view.findViewById(R.id.share_img);
            post_img = (ImageView) view.findViewById(R.id.post_img);
            webView = (WebView) view.findViewById(R.id.webview);
            liked_img = (ImageView) view.findViewById(R.id.liked_image);
            read_more = (TextView) view.findViewById(R.id.expand_text);

            post_details.setAnimationDuration(100L);

            // set interpolators for both expanding and collapsing animations
            post_details.setInterpolator(new OvershootInterpolator());

            // or set them separately
            post_details.setExpandInterpolator(new OvershootInterpolator());
            post_details.setCollapseInterpolator(new OvershootInterpolator());

            // toggle the ExpandableTextView
            read_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    post_details.toggle();
                    read_more.setText(post_details.isExpanded() ? R.string.collapse : R.string.expand);
                }
            });


            read_more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    if (post_details.isExpanded()) {
                        post_details.collapse();
                        read_more.setText(R.string.expand);
                    } else {
                        post_details.expand();
                        read_more.setText(R.string.collapse);
                    }
                }
            });
            // listen for expand / collapse events
            post_details.setOnExpandListener(new ExpandableTextView.OnExpandListener() {
                @Override
                public void onExpand(final ExpandableTextView view) {
                    Log.d("", "ExpandableTextView expanded");
                }

                @Override
                public void onCollapse(final ExpandableTextView view) {
                    Log.d("", "ExpandableTextView collapsed");
                }
            });


            Typeface font98 = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
            read_more.setTypeface(font98);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
            post_heading.setTypeface(font);
            Typeface font1 = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
            date_time.setTypeface(font1);
            Typeface font2 = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
            like_count.setTypeface(font2);
            Typeface font3 = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
            share_count.setTypeface(font3);
            Typeface font4 = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
            comment_count.setTypeface(font4);
            Typeface font6 = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
            post_details.setTypeface(font6);
            comment_text = (TextView) view.findViewById(R.id.comment_text);
            Typeface font5 = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
            comment_text.setTypeface(font5);
            comment_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    post = post_data.get(pos);
                    int post_id = post.getId();
                    Intent intent = new Intent(context, CommentActivity.class);
                    intent.putExtra("post_id", post_id);
                    context.startActivity(intent);
                }
            });

            like_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    like_img.setClickable(true);
                    int pos = getAdapterPosition();
                    post = post_data.get(pos);
                    int id = post.getId();
                    String isliked = post.getIs_liked();
                    String postname = post.getTitle();

                    if (isliked.equals("true")) {

                        Toast.makeText(context, "You already liked this Post", Toast.LENGTH_LONG).show();
                    } else if (isliked.equals("false")) {
                        Toast.makeText(context, "You liked" + " " + postname+ "Post's", Toast.LENGTH_SHORT).show();
                        String present_value = like_count.getText().toString();
                        int present_value_new = Integer.parseInt(present_value);
                        present_value_new++;
                        like_count.setText(String.valueOf(present_value_new));
                        likeapi(id);
                        like_img.setVisibility(View.GONE);
                        liked_img.setVisibility(View.VISIBLE);
                        like_img.setClickable(false);
                    }


                }
            });

            share_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    //sharing implementation here
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Schulerz");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Schulerz is a smart and interactive management system for institutions helping them to manage complex functions like fees, attendance, results, announcements and much more. It also helps in keeping the parents informed and impressed. clicke here to visit http://www.shiksha.com/ ");
                    context.startActivity(Intent.createChooser(sharingIntent, "Share via"));
                    int pos = getAdapterPosition();
                    post = post_data.get(pos);
                    int id = post.getId();
                    String present_value = share_count.getText().toString();
                    int present_value_new = Integer.parseInt(present_value);
                    present_value_new++;
                    share_count.setText(String.valueOf(present_value_new));
                    shareapi(id);
                }
            });

            comment_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    post = post_data.get(pos);
                    int post_id = post.getId();
                    Intent intent = new Intent(context, CommentActivity.class);
                    intent.putExtra("post_id", post_id);
                    context.startActivity(intent);


                }
            });


        }


    }


}
