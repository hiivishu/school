package app.com.shiksha.adapter;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.activity.ViewPdfActivity;
import app.com.shiksha.model.Syllabus;

/**
 * Created by Vishu on 30/09/17.
 */

public class SyllabusAdapter extends RecyclerView.Adapter<SyllabusAdapter.ViewHolder> {

    public List<Syllabus> syllabus_data;
    private Context context;

    public SyllabusAdapter(Context context, List<Syllabus> syllabus_data) {
        this.syllabus_data = syllabus_data;
        this.context = context;
    }

    @Override
    public SyllabusAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.syllabus_lay, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SyllabusAdapter.ViewHolder holder, int position) {
        final Syllabus syllabus = getItem(position);
//        holder.class_syllabus.setText(String.valueOf("Class :"+" "+syllabus.getClassId()));
//        Picasso.with(context)
//                .load("http://koshishindia.co.in/"+syllabus.getContentFile())
//                .placeholder(R.color.simplegray) //this is optional the image to display while the url image is downloading
//                .into(holder.fileImageSyllabus);

        holder.download_syllabus_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("click","yes");
                String pdfUrl = syllabus.getContentFile();

                Intent intent = new Intent(context, ViewPdfActivity.class);
                intent.putExtra("pdfUrl",pdfUrl);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
//                try {
//                    DownloadManager.Request request = new DownloadManager.Request(Uri.parse(pdfUrl));
//                    request.setDescription("Downloading Syllabus");
//                    request.setTitle("Syllabus");
//                    Toast.makeText(context, "Downloading Started", Toast.LENGTH_LONG).show();
//// in order for this if to run, you must use the android 3.2 to compile your app
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
//                        request.allowScanningByMediaScanner();
//                        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//                    }
//                    request.setDestinationInExternalPublicDir("Shiksha/", "Syllabus" + ".pdf");
//
//// get download service and enqueue file
//                    DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
//                    manager.enqueue(request);
//                } catch (Throwable throwable) {
//
//                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return syllabus_data.size();
    }

    private Syllabus getItem(int position) {
        return position < syllabus_data.size() ? syllabus_data.get(position) : null;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout download_syllabus_lay;

        public ViewHolder(View itemView) {
            super(itemView);
            download_syllabus_lay = (RelativeLayout) itemView.findViewById(R.id.download_syllabus_lay);

        }
    }
}
