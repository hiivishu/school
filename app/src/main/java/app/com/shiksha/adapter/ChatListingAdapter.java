package app.com.shiksha.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.com.shiksha.R;
import app.com.shiksha.activity.ChatRoomActivity;
import app.com.shiksha.model.Chat;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by vishal on 1/11/17.
 */

public class ChatListingAdapter extends RecyclerView.Adapter<ChatListingAdapter.ViewHolder> {

    private Context context;
    private List<Chat> chat_data;
    SharedPreferences sharedPreferences = null;
    SharedPreferences.Editor editor;
    int user_id;
    String gurdian_id;
    String type;
    String room;

    public ChatListingAdapter(Context context, List<Chat> chat_data) {
        this.context= context;
        this.chat_data=chat_data;


    }

    @Override
    public ChatListingAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_listing_lay, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ChatListingAdapter.ViewHolder holder, int position) {
        Chat chat = chat_data.get(position);
        holder.textView.setText(chat.getName_teacher());
        Picasso.with(context)
                .load(chat.getTeacher_image_url())
                .placeholder(R.color.simplegray) //this is optional the image to display while the url image is downloading
                .into(holder.circleImageView);

        String na = "Ashis Singh";
        if (chat.getName_teacher().equals(na)){
            int pos = position;
            Log.e("cou",""+pos);
            holder.newchat.setVisibility(View.INVISIBLE);
            Collections.swap(chat_data,0,pos);
            Collections.reverseOrder();

//           notifyItemMoved(pos,0);
        }

    }

    @Override
    public int getItemCount() {

        return chat_data.size();

    }
//    // Swap itemA with itemB
//    public void swapItems(int itemAIndex, int itemBIndex) {
//        //make sure to check if dataset is null and if itemA and itemB are valid indexes.
//        String itemA = chat_data.get(itemAIndex);
//        String itemB = dataset.get(itemBIndex);
//        dataset.set(itemAIndex, itemB);
//        dataset.set(itemBIndex, ItemA);
//
//        notifyDataSetChanged(); //This will trigger onBindViewHolder method from the adapter.
//    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView circleImageView;
        TextView textView;
        RelativeLayout relativeLayout;
        Chat chat;
        ImageView newchat;
        private DatabaseReference root = FirebaseDatabase.getInstance().getReference();
        public ViewHolder(final View view) {
            super(view);
            sharedPreferences = context.getSharedPreferences("Login_pref", 0);
            editor = sharedPreferences.edit();
            user_id = sharedPreferences.getInt("id", 0);
            gurdian_id = String.valueOf(user_id);
            type = sharedPreferences.getString("type","");


            circleImageView = (CircleImageView)view.findViewById(R.id.student_pic1_chat_listing);
            textView = (TextView)view.findViewById(R.id.person_name1_chat_listing);
            relativeLayout = (RelativeLayout)view.findViewById(R.id.chat_lay);
            newchat = (ImageView)view.findViewById(R.id.newchat);
            String na ="Ashis Singh";
//


//                if (chat.getName_teacher().equals(na)){
//
//                }




            relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    int pos = getAdapterPosition();
                    chat = chat_data.get(pos);
                    int id = chat.getId();
                    newchat.setVisibility(View.INVISIBLE);
                    if (type.equals("Guardian")){
                        room = gurdian_id+"&"+id;
                        root.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot.hasChild(room)){

                                }else {
                                    Map<String,Object> map =  new HashMap<String, Object>();
                                    map.put(room,"");
                                    root.updateChildren(map);
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                    }else {
                        room = id+"&"+gurdian_id;
                        root.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot.hasChild(room)){

                                }else {
                                    Map<String,Object> map =  new HashMap<String, Object>();
                                    map.put(room,"");
                                    root.updateChildren(map);
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
                    }

                    Intent intent = new Intent(context, ChatRoomActivity.class);
                    intent.putExtra("person_name",chat.getName_teacher());
                    intent.putExtra("person_image",chat.getTeacher_image_url());
                    intent.putExtra("room_name",room);


                    context.startActivity(intent);


                }
            });

        }


        }

    }
