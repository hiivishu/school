package app.com.shiksha.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.model.Result;

/**
 * Created by vishal on 12/3/16.
 */

public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.ViewHolder> {

    public List<Result> result_data;
    private Context context;
    private View view1;

    public ResultAdapter(Context context, List<Result> result_data) {
        this.result_data = result_data;
        this.context = context;
    }

    @Override
    public ResultAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.result_lay, parent, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(ResultAdapter.ViewHolder holder, int position) {

        Result result = result_data.get(position);

        holder.subject_name.setText(result.getSubject_name());
        holder.details_marks_obtain.setText(String.valueOf(result.getMarks_obtained()));
        holder.totalmarksvalue1.setText(String.valueOf(result.getMax_marks()));
        holder.exam_name.setText(result.getExam_name());
        if (result.getGrade().equals("B")){
            view1.setBackgroundResource(R.color.result_corner);
            holder.subject_grade.setText(result.getGrade());
            holder.subject_grade.setTextColor(ColorStateList.valueOf(Color.argb(255, 174, 0, 1)));
        }else {
            holder.subject_grade.setText(result.getGrade());
        }

    }

    @Override
    public int getItemCount() {
        return result_data.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView subject_name, details_marks_obtain, totalmarksvalue1, subject_grade,exam_name;

        public ViewHolder(final View view) {
            super(view);
            subject_name = (TextView) view.findViewById(R.id.subject_name);
            details_marks_obtain = (TextView) view.findViewById(R.id.details_marks_obtain);
            totalmarksvalue1 = (TextView) view.findViewById(R.id.totalmarksvalue1);
            subject_grade = (TextView) view.findViewById(R.id.subject_grade);
            exam_name = (TextView)view.findViewById(R.id.exam_name);
            view1 = view.findViewById(R.id.viewid);

            TextView textView = (TextView) view.findViewById(R.id.subject);
            Typeface font1 = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            textView.setTypeface(font1);
            TextView textView1 = (TextView) view.findViewById(R.id.marks1);
            Typeface font3 = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            textView1.setTypeface(font3);
            TextView textView2 = (TextView) view.findViewById(R.id.totalmarks1);
            Typeface font9 = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            textView2.setTypeface(font9);

            Typeface font5 = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            subject_grade.setTypeface(font5);

            Typeface font7 = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            totalmarksvalue1.setTypeface(font7);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            details_marks_obtain.setTypeface(font);
            Typeface font2 = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            subject_name.setTypeface(font2);

            Typeface font91 = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            exam_name.setTypeface(font91);


        }
    }
}
