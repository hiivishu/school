package app.com.shiksha.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.model.Comment;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by vishal on 11/18/16.
 */

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder>  {

    public List<Comment> comment_data;
    private Context context;

    public CommentAdapter(Context context, List<Comment> post_data){
        this.comment_data = post_data;
        this.context = context;
    }

    @Override
    public CommentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_lay,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
      Comment comment = comment_data.get(position);

        holder.comment_date_time.setText(comment.getCreated_at());
        holder.comment_details.setText(comment.getComment());
        holder.comment_person_name.setText(comment.getGuardian_name());
        Picasso.with(context)
                .load(comment.getGuardian_image_url())
                .placeholder(R.color.simplegray) //this is optional the image to display while the url image is downloading
                .into(holder.circleImageView);



    }


    @Override
    public int getItemCount() {
        return comment_data.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder{
          TextView comment_person_name,comment_details,comment_date_time;
        CircleImageView circleImageView;

        public ViewHolder(final View view) {
            super(view);
            comment_person_name = (TextView)view.findViewById(R.id.comment_person_name);
            comment_details = (TextView)view.findViewById(R.id.comment_details);
            comment_date_time = (TextView)view.findViewById(R.id.comment_date_time);
            circleImageView = (CircleImageView)view.findViewById(R.id.pro_pic);
            Typeface font1 = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
            comment_person_name.setTypeface(font1);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
            comment_details.setTypeface(font);
            Typeface font2 = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
            comment_date_time.setTypeface(font2);
        }
    }

}
