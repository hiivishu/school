package app.com.shiksha.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.model.GurdainChildView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by vishal on 2/10/17.
 */

public class GurdianChildViewAdapter extends RecyclerView.Adapter<GurdianChildViewAdapter.ViewHolder> {

    public List<GurdainChildView> gurdianchild_data;
    private Context context;

    public GurdianChildViewAdapter(Context context, List<GurdainChildView> gurdianchild_data){
        this.gurdianchild_data = gurdianchild_data;
        this.context = context;
    }

    @Override
    public GurdianChildViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gurdian_child_view,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GurdianChildViewAdapter.ViewHolder holder, int position) {

        GurdainChildView gurdainChildView = gurdianchild_data.get(position);
        holder.child_name.setText(gurdainChildView.getName());
        if (gurdainChildView.getName().equals("Vishal")){
            Picasso.with(context)
                    .load(R.drawable.child1)
                    .placeholder(R.drawable.default_avatar) //this is optional the image to display while the url image is downloading
                    .into(holder.circleImageView);
        }else if (gurdainChildView.getName().equals("Vivek")){
            Picasso.with(context)
                    .load(R.drawable.chil2)
                    .placeholder(R.drawable.default_avatar) //this is optional the image to display while the url image is downloading
                    .into(holder.circleImageView);
        }else {
            Picasso.with(context)
                    .load(R.drawable.default_avatar)
                    .placeholder(R.drawable.default_avatar) //this is optional the image to display while the url image is downloading
                    .into(holder.circleImageView);
        }
    }

    @Override
    public int getItemCount() {
        return gurdianchild_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView circleImageView;
        TextView child_name;
        public ViewHolder(final View view) {
            super(view);
            circleImageView = (CircleImageView)view.findViewById(R.id.gurdian_child_pic);
            child_name = (TextView)view.findViewById(R.id.gurdian_child_name);
            Typeface font6 = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            child_name.setTypeface(font6);
        }
    }
}
