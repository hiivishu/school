package app.com.shiksha.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.model.FeeData;

/**
 * Created by vishal on 12/1/16.
 */

public class FeeDataAdapter extends RecyclerView.Adapter<FeeDataAdapter.ViewHolder> {


    public List<FeeData> fee_data;
    private Context context;

    public FeeDataAdapter(Context context, List<FeeData> fee_data){
        this.fee_data = fee_data;
        this.context = context;
    }

    @Override
    public FeeDataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fee_lay,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FeeDataAdapter.ViewHolder holder, int position) {

        FeeData feeData = fee_data.get(position);

        holder.textView.setText(feeData.getRemarks());
        holder.textView1.setText(feeData.getSubmission_date());
        Log.e("adap",feeData.getRemarks());
    }

    @Override
    public int getItemCount() {

        Log.e("size",""+fee_data.size());
        return fee_data.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView,textView1;

        public ViewHolder(final View view) {
            super(view);

            textView = (TextView)view.findViewById(R.id.paidfee);
            textView1 = (TextView)view.findViewById(R.id.paid_date_fee);

            Typeface font5 = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
            textView.setTypeface(font5);

            Typeface font7 = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Regular.ttf");
            textView1.setTypeface(font7);

        }


    }


}
