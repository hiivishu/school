package app.com.shiksha.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.model.ClassMate;

/**
 * Created by Vishu on 25/02/18.
 */

public class ClassMateAdapter extends RecyclerView.Adapter<ClassMateAdapter.ViewHolder> {

    private List<ClassMate> classMates_data;
    private Context context;

    public ClassMateAdapter(Context context, List<ClassMate> classMates_data) {

        this.context = context;
        this.classMates_data = classMates_data;
    }

    @Override
    public ClassMateAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.classmate_lay, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ClassMateAdapter.ViewHolder holder, int position) {
        final ClassMate classMate = getItem(position);
        Picasso.with(context)
                .load(classMate.getImagePath())
                .placeholder(R.color.simplegray) //this is optional the image to display while the url image is downloading
                .into(holder.classmate_img);
        holder.classmate_dob.setText(classMate.getDOB());
        holder.classmate_name.setText(classMate.getStudentName());
        holder.classmate_student_id.setText(classMate.getStudentId());

    }

    @Override
    public int getItemCount() {
        return classMates_data.size();
    }

    private ClassMate getItem(int position) {
        return position < classMates_data.size() ? classMates_data.get(position) : null;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView classmate_img;
        TextView classmate_name, classmate_student_id, classmate_dob;

        public ViewHolder(final View view) {
            super(view);

            classmate_img = (ImageView)view.findViewById(R.id.classmate_img);
            classmate_name = (TextView)view.findViewById(R.id.classmate_name);
            classmate_student_id = (TextView)view.findViewById(R.id.classmate_student_id);
            classmate_dob = (TextView)view.findViewById(R.id.classmate_dob);
        }

    }
}
