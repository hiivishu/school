package app.com.shiksha.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.model.Holiday_List;

/**
 * Created by vishal on 1/21/17.
 */

public class HolidayListAdapter extends RecyclerView.Adapter<HolidayListAdapter.ViewHolder> {

    private List<Holiday_List> holiday_data;
    private Context context;

    public HolidayListAdapter(Context context, List<Holiday_List> holiday_data) {

        this.context = context;
        this.holiday_data = holiday_data;
    }


    @Override
    public HolidayListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.holiday_list_lay, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HolidayListAdapter.ViewHolder holder, int position) {
        Holiday_List holiday_list = holiday_data.get(position);
        holder.holiday_name.setText(holiday_list.getName());
        holder.holiday_date.setText(holiday_list.getDate());



    }

    @Override
    public int getItemCount() {
        return holiday_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView holiday_name,holiday_date;
        public ViewHolder(final View view) {
            super(view);

            holiday_date = (TextView)view.findViewById(R.id.holiday_date);
            holiday_name = (TextView)view.findViewById(R.id.holiday_name);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            holiday_name.setTypeface(font);
            Typeface font1 = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            holiday_date.setTypeface(font1);

        }
    }
}
