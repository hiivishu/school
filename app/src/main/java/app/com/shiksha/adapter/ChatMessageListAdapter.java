package app.com.shiksha.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.model.Chat_room_msg;

/**
 * Created by vishal on 1/16/17.
 */

public class ChatMessageListAdapter extends RecyclerView.Adapter<ChatMessageListAdapter.ViewHolder> {


    private List<Chat_room_msg> chat_room_msgs;
    private Context context;

    public ChatMessageListAdapter(Context context, ArrayList<Chat_room_msg> chat_data) {
        this.context =context;
        this.chat_room_msgs = chat_data;


    }

    @Override
    public ChatMessageListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_single_design,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ChatMessageListAdapter.ViewHolder holder, int position) {
        Chat_room_msg chat_room_msg = chat_room_msgs.get(position);
        holder.textView.setText(chat_room_msg.getMsg());

    }

    @Override
    public int getItemCount() {
        return chat_room_msgs.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView textView;
        public ViewHolder(final View view) {
            super(view);

            textView = (TextView)view.findViewById(R.id.chat_data_view);
        }

    }
}
