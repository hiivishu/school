package app.com.shiksha.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.activity.ViewPdfActivity;
import app.com.shiksha.model.LatestTimeTable;
import app.com.shiksha.model.NewTImeTable;

/**
 * Created by Vishu on 30/09/17.
 */

public class NewTimeTableAdapter extends RecyclerView.Adapter<NewTimeTableAdapter.ViewHolder> {
    public List<LatestTimeTable> tImeTableList_data;
    private Context context;
    private String className;

    public NewTimeTableAdapter(Context context, List<LatestTimeTable> tImeTableList_data,String className) {
        this.tImeTableList_data = tImeTableList_data;
        this.context = context;
        this.className = className;
    }

    @Override
    public NewTimeTableAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_timetable_lay, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(NewTimeTableAdapter.ViewHolder holder, int position) {

//        NewTImeTable newTImeTable = tImeTableList_data.get(position);
//        holder.dayTimeTable.setText(newTImeTable.getDay());
//        holder.fromTimeTable.setText(newTImeTable.getFromTime());
//        holder.totimeTable.setText(newTImeTable.getToTime());
//        holder.subjectTimeTable.setText(newTImeTable.getSubject());

        final LatestTimeTable latestTimeTable = getItem(position);
        holder.timeTableFilePath.setText("TimeTable of Class: "+className);
        holder.viewTimeTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, ViewPdfActivity.class);
                intent.putExtra("pdfUrl",latestTimeTable.getFilePath());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

            }
        });


    }

    @Override
    public int getItemCount() {
        return tImeTableList_data.size();
    }

    private LatestTimeTable getItem(int position) {
        return position < tImeTableList_data.size() ? tImeTableList_data.get(position) : null;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView dayTimeTable, fromTimeTable, subjectTimeTable, totimeTable,timeTableFilePath;
        Button viewTimeTable;

        public ViewHolder(View itemView) {
            super(itemView);

            viewTimeTable = (Button)itemView.findViewById(R.id.viewTimeTable);
            timeTableFilePath = (TextView)itemView.findViewById(R.id.timeTableFilePath);
//            dayTimeTable = (TextView)itemView.findViewById(R.id.dayTimeTable);
//            fromTimeTable = (TextView)itemView.findViewById(R.id.fromTimeTable);
//            subjectTimeTable = (TextView)itemView.findViewById(R.id.subjectTimeTable);
//            totimeTable = (TextView)itemView.findViewById(R.id.totimeTable);

        }
    }

}
