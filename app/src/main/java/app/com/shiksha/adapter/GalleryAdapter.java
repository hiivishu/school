package app.com.shiksha.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.activity.ImageDetailsActivity;
import app.com.shiksha.model.Gallery;

/**
 * Created by vishal on 12/26/16.
 */

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    public List<Gallery> gallery_data;
    private Context context;


    public GalleryAdapter(Context context, List<Gallery> gallery_data) {

        this.context = context;
        this.gallery_data = gallery_data;


    }

    @Override
    public GalleryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_lay, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GalleryAdapter.ViewHolder holder, int position) {


        final Gallery gallery = getItem(position);
        Picasso.with(context)
                .load("http://ccclucknow.com/ccclucknow/"+gallery.getGallery_image_url())
                .placeholder(R.color.simplegray) //this is optional the image to display while the url image is downloading
                .into(holder.img);
        Log.e("PhotoUrl","http://ccclucknow.com/ccclucknow/"+gallery.getGallery_image_url());
        holder.caption.setText(gallery.getDescription());
        holder.heading.setText(gallery.getTitle());
        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,ImageDetailsActivity.class);
                intent.putExtra("Title",gallery.getTitle());
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return gallery_data.size();

    }

    private Gallery getItem(int position) {
        return position < gallery_data.size() ? gallery_data.get(position) : null;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView heading, caption;
        ImageView img;


        Gallery gallery;

        public ViewHolder(final View view) {
            super(view);

            heading = (TextView) view.findViewById(R.id.headingtext);
            caption = (TextView) view.findViewById(R.id.gallery_caption);
            img = (ImageView) view.findViewById(R.id.img_gallery);
//            zoominanimation = AnimationUtils.loadAnimation(context, R.anim.zoom_in);
//            zoomoutanimation = AnimationUtils.loadAnimation(context, R.anim.zoom_out);

            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            heading.setTypeface(font);
            heading.setAllCaps(true);
            caption.setAllCaps(true);
            Typeface font1 = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            caption.setTypeface(font1);
//            img.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    int pos = getAdapterPosition();
//                    gallery = gallery_data.get(pos);
//                    String img = gallery.getGallery_image_url();
//                    String des = gallery.getDescription();
//                    int id = gallery.getId();
//                    Intent intent = new Intent(context, ImageDetailsActivity.class);
//                    intent.putExtra("img_id", id);
//                    context.startActivity(intent);
//                }
//            });

        }

    }
}
