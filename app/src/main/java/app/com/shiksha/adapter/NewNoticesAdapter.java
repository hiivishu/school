package app.com.shiksha.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.model.NewNotices;

/**
 * Created by vishal on 12/12/16.
 */

public class NewNoticesAdapter extends RecyclerView.Adapter<NewNoticesAdapter.ViewHolder>  {
    public List<NewNotices> notices_data;
    private Context context;

    public NewNoticesAdapter(Context context, List<NewNotices> notices_data) {
        this.notices_data = notices_data;
        this.context = context;
    }


    @Override
    public NewNoticesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.new_notices_lay, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NewNoticesAdapter.ViewHolder holder, int position) {
        NewNotices newNotices = notices_data.get(position);
        holder.textView.setText(newNotices.getSubject());
        holder.textView1.setText(newNotices.getDescription());

    }

    @Override
    public int getItemCount() {
        return notices_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView textView,textView1;
        public ViewHolder(final View view) {
            super(view);
            textView = (TextView)view.findViewById(R.id.notices_heading);
            textView1 = (TextView)view.findViewById(R.id.notices_subheading);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
        textView.setTypeface(font);
            Typeface font1 = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            textView1.setTypeface(font1);
        }
    }

    }
