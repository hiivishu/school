package app.com.shiksha.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.model.GalleryDetail;

/**
 * Created by Vishu on 25/10/17.
 */

public class GalleryDetailAdapter extends RecyclerView.Adapter<GalleryDetailAdapter.ViewHolder> {

    public List<GalleryDetail> gallery_data;
    private Context context;

    public GalleryDetailAdapter(Context context, List<GalleryDetail> gallery_data) {

        this.context = context;
        this.gallery_data = gallery_data;
    }


    @Override
    public GalleryDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gallery_detail_lay, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GalleryDetailAdapter.ViewHolder holder, int position) {
        GalleryDetail galleryDetail = gallery_data.get(position);
        Picasso.with(context)
                .load("http://ccclucknow.com/ccclucknow/"+galleryDetail.getImage())
                .placeholder(R.color.simplegray) //this is optional the image to display while the url image is downloading
                .into(holder.galleryDeImage);

    }

    @Override
    public int getItemCount() {
        return gallery_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView galleryDeImage;
        public ViewHolder(View itemView) {
            super(itemView);
            galleryDeImage = (ImageView)itemView.findViewById(R.id.galleryDeImage);
        }
    }
}
