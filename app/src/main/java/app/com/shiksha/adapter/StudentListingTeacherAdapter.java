package app.com.shiksha.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.model.StudentListing;

/**
 * Created by vishal on 2/14/17.
 */

public class StudentListingTeacherAdapter extends RecyclerView.Adapter<StudentListingTeacherAdapter.ViewHolder> {

    private List<StudentListing> studentListings_data;
    private Context context;
    public static String attendancecount;

    public StudentListingTeacherAdapter(Context context, List<StudentListing> studentListings_data) {

        this.context = context;
        this.studentListings_data = studentListings_data;
    }

    @Override
    public StudentListingTeacherAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.student_listing_lay, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(StudentListingTeacherAdapter.ViewHolder holder, int position) {
        StudentListing studentListing = studentListings_data.get(position);
        holder.student_name.setText(studentListing.getStudent_name());

    }

    @Override
    public int getItemCount() {
        return studentListings_data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView student_name;
        SwitchCompat attendance_student_switch;
        StudentListing student;
        ArrayList<String> attendancePresent;
        ArrayList<String> attendanceAbsent;


        public ViewHolder(final View view) {
            super(view);
            student_name = (TextView) view.findViewById(R.id.student_name);
            attendance_student_switch = (SwitchCompat) view.findViewById(R.id.attendance_student_switch);
            Typeface font1 = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            student_name.setTypeface(font1);
            attendance_student_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    Log.e("ischechked", "" + isChecked);
                    attendancePresent = new ArrayList<String>();
                    int pos = getAdapterPosition();
                    student = studentListings_data.get(pos);
                    String nam = student.getStudent_name();

                    String student;
                    if (isChecked) {

                        student = "P";
                        Log.e("checknew", nam + " " + student);
//                      String finalAttendance = nam+" "+student;



                    } else if (!isChecked) {
                        student = "A";
                        Log.e("checknew", nam + " " + student);
                        String finalAttendancesecond = nam+" "+student;




                    }




                }

            });


        }
    }
}
