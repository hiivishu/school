package app.com.shiksha.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.helper.UrlSupport;
import app.com.shiksha.model.NewEvents;

/**
 * Created by vishal on 12/7/16.
 */

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.ViewHolder> {
    public List<NewEvents> events_data;
    SharedPreferences sharedPreferences = null;
    SharedPreferences.Editor editor;
    private ProgressDialog progressDialog;
    int user_id;
    String gurdian_id;
    private Context context;

    public EventsAdapter(Context context, List<NewEvents> events_data) {
        this.events_data = events_data;
        this.context = context;
    }

    @Override
    public EventsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_lay_invited, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EventsAdapter.ViewHolder holder, int position) {

        NewEvents newEvents = events_data.get(position);
        holder.invited_heading.setText(String.valueOf(newEvents.getInvitedevent_name()));
        holder.date.setText(String.valueOf(newEvents.getInvitedcreated_at()));
        holder.venuevenue.setText(String.valueOf(newEvents.getInvitedevent_place()));
        Picasso.with(context)
                .load(newEvents.getInvitedevent_image_url())
                .placeholder(R.color.simplegray) //this is optional the image to display while the url image is downloading
                .into(holder.invited_img);
    }

    @Override
    public int getItemCount() {
        return events_data.size();
    }

    private void EventAcceptApi(int id) {


        sharedPreferences = context.getSharedPreferences("Login_pref", 0);
        editor = sharedPreferences.edit();

        user_id = sharedPreferences.getInt("id", 0);

        gurdian_id = String.valueOf(user_id);

        Log.e("acceptid",gurdian_id);


        StringRequest stringRequest = new StringRequest(Request.Method.GET, UrlSupport.EventAcceptApi + gurdian_id + "&event_id=" + id,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("EventsAcceptResponse", "" + response);

                        try {
                            JSONObject jsonobject = new JSONObject(response);

                            String success = jsonobject.getString("success");
                            if (success.equals("true")){





                                Toast.makeText(context,"You Accepted Invitation Kindly Refresh ",Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView invited_img;
        TextView invited_heading, date, venuevenue;

        NewEvents newEvents;

        public ViewHolder(final View view) {
            super(view);

            invited_img = (ImageView) view.findViewById(R.id.invited_img);
            invited_heading = (TextView) view.findViewById(R.id.invited_heading);
            date = (TextView) view.findViewById(R.id.date);
            venuevenue = (TextView) view.findViewById(R.id.venue);
            Typeface font5 = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            invited_heading.setTypeface(font5);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            date.setTypeface(font);
            Typeface font2 = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            venuevenue.setTypeface(font2);

            Button button = (Button) view.findViewById(R.id.acceptbutton);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    newEvents = events_data.get(pos);
                    int id = newEvents.getInvitedid();

                    EventAcceptApi(id);
                    

                }
            });
            Typeface font4 = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            button.setTypeface(font4);
            Button button1 = (Button) view.findViewById(R.id.declinebutton);
            Typeface font7 = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            button1.setTypeface(font7);


        }

    }


}
