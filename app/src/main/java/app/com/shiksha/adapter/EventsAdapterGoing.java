package app.com.shiksha.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.model.NewEventsgoing;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by vishal on 12/7/16.
 */

public class EventsAdapterGoing extends RecyclerView.Adapter<EventsAdapterGoing.ViewHolder> {

    private LayoutInflater lauoutInflater;
    public List<NewEventsgoing> event_going;
    private Context context;

    public EventsAdapterGoing(Context context, List<NewEventsgoing> event_going) {
        lauoutInflater = LayoutInflater.from(context);
        this.event_going = event_going;
        this.context = context;
    }

    @Override
    public EventsAdapterGoing.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_lay_going, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EventsAdapterGoing.ViewHolder holder, int position) {

        NewEventsgoing newEvents = event_going.get(position);
        holder.invited_heading.setText(String.valueOf(newEvents.getEventName()));
        holder.date.setText(String.valueOf(newEvents.getEventDate()));
//        holder.venuevenue.setText(String.valueOf(newEvents.getEventPlace()));
        holder.des.setText(newEvents.getDescription());
//        Picasso.with(context)
//                .load("http://www.koshishindia.co.in"+newEvents.getFileUpload())
//                .placeholder(R.color.darker_gray) //this is optional the image to display while the url image is downloading
//                .into(holder.invited_img);

    }

    @Override
    public int getItemCount() {
        return event_going.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView invited_img;
        TextView invited_heading, date, venuevenue,noofgoing,areattendingtext,des;
        RelativeLayout relativeLayout;
        CircleImageView circleImageView;
        RecyclerView recyclerViewchild;

        public ViewHolder(final View view) {
            super(view);

            invited_img = (ImageView) view.findViewById(R.id.going_header_img);
            invited_heading = (TextView) view.findViewById(R.id.headingname2);
            date = (TextView) view.findViewById(R.id.date2);
            venuevenue = (TextView) view.findViewById(R.id.venue2);
            noofgoing = (TextView)view.findViewById(R.id.noofgoing);
            des = (TextView)view.findViewById(R.id.des);
            areattendingtext = (TextView)view.findViewById(R.id.areattendingtext);
            circleImageView = (CircleImageView)view.findViewById(R.id.student_pic_e);
             recyclerViewchild = (RecyclerView)view.findViewById(R.id.event_pic_recyccle);


            Typeface font21 = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            areattendingtext.setTypeface(font21);
            Typeface font22 = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            noofgoing.setTypeface(font22);
            Typeface font5 = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            invited_heading.setTypeface(font5);
            Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            date.setTypeface(font);
            Typeface font2 = Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
            venuevenue.setTypeface(font2);




        }

    }


}
