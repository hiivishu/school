package app.com.shiksha;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.List;

import app.com.shiksha.activity.LoginActivity;
import app.com.shiksha.activity.ReportProblemActivity;
import app.com.shiksha.fragment.DashboardFragment;
import app.com.shiksha.fragment.HomeFragment;
import app.com.shiksha.helper.UrlSupport;
import sidemenu.ResideMenu;
import sidemenu.ResideMenuItem;

public class MainActivity extends FragmentActivity implements View.OnClickListener {
    public static ViewPager viewPager = null;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    int id;
    boolean ischecked = false;
    boolean isIschecked1 = false;
    boolean isIschecked2 = false;
    ViewPagerAdapter adapter;
    Animation zoominanimation;
    Animation zoomoutanimation;
    String type;
    private ResideMenu resideMenu;
    private MainActivity mContext;
    private ResideMenuItem itemViewProfile;
    private ResideMenuItem itemAddStudent;
    private ResideMenuItem itemHelp;
    private ResideMenuItem itemReportProblem;
    private ResideMenuItem itemLogout;
    private ProgressDialog progressDialog;
    private TextView textView;
    private String email,password;
    private ImageButton post, home, message;
    private ResideMenu.OnMenuListener menuListener = new ResideMenu.OnMenuListener() {
        @Override
        public void openMenu() {
//            Toast.makeText(mContext, "Menu is opened!", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void closeMenu() {
//            Toast.makeText(mContext, "Menu is closed!", Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setContentView(R.layout.activity_main);

        mContext = this;
        setUpMenu();
        if (savedInstanceState == null)
            changeFragment(new HomeFragment());
        sharedPreferences = getSharedPreferences("Login_pref", 0);
        editor = sharedPreferences.edit();
        email = getIntent().getStringExtra("emailData");
        password = getIntent().getStringExtra("passwordData");
        id = sharedPreferences.getInt("id", 0);
        type = sharedPreferences.getString("type", "");
        textView = (TextView) findViewById(R.id.page_indicator);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textView.setTypeface(font);
        post = (ImageButton) findViewById(R.id.post_image);
        home = (ImageButton) findViewById(R.id.home_image);
        message = (ImageButton) findViewById(R.id.message_image);
        Log.e("mainid", "" + id);
//        Button button = (Button) findViewById(R.id.notification);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
//                startActivity(intent);
//                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
//
//            }
//        });
//        zoominanimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
//        zoomoutanimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_out);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
//        viewPager.setOffscreenPageLimit(3);
    }
////        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
////            @Override
////            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
////                switch (position) {
////                    case 0:
////                        textView.setText("POST");
////                        return;
////                    case 1:
////                        if (type.equals("Teacher")) {
////                            textView.setText("ATTENDANCE");
////                        } else {
////                            textView.setText("HOME");
////                        }
////
////                        return;
////                    case 2:
////                        textView.setText("MESSAGE");
////                        return;
////                }
////                return;
////            }
//
//            @Override
//            public void onPageSelected(int position) {
//                viewPager.setCurrentItem(position);
//                if (position == 0) {
//                    if (type.equals("Teacher")) {
//                        post.setImageResource(R.drawable.rsz_posts_selected);
//                        post.setScaleType(ImageView.ScaleType.CENTER);
//                        post.startAnimation(zoominanimation);
//                        home.startAnimation(zoomoutanimation);
//                        message.startAnimation(zoomoutanimation);
//                        home.setImageResource(R.drawable.ic_insert_invitation_black_24dp);
//                        home.setScaleType(ImageView.ScaleType.CENTER);
//                        message.setImageResource(R.drawable.rsz_messages_deselected);
//                        message.setScaleType(ImageView.ScaleType.CENTER);
//                    } else {
//                        post.setImageResource(R.drawable.rsz_posts_selected);
//                        post.setScaleType(ImageView.ScaleType.CENTER);
//                        post.startAnimation(zoominanimation);
//                        home.startAnimation(zoomoutanimation);
//                        message.startAnimation(zoomoutanimation);
//                        home.setImageResource(R.drawable.rsz_home_unselected);
//                        home.setScaleType(ImageView.ScaleType.CENTER);
//                        message.setImageResource(R.drawable.rsz_messages_deselected);
//                        message.setScaleType(ImageView.ScaleType.CENTER);
//                    }
//                } else if (position == 1) {
//                    if (type.equals("Teacher")) {
//                        post.setImageResource(R.drawable.rsz_posts_deselected);
//                        post.setScaleType(ImageView.ScaleType.CENTER);
//                        home.startAnimation(zoominanimation);
//                        post.startAnimation(zoomoutanimation);
//                        message.startAnimation(zoomoutanimation);
//                        home.setImageResource(R.drawable.ic_insert_invitation_black_24dp);
//                        home.setScaleType(ImageView.ScaleType.CENTER);
//                        message.setImageResource(R.drawable.rsz_messages_deselected);
//                        message.setScaleType(ImageView.ScaleType.CENTER);
//                    } else {
//                        post.setImageResource(R.drawable.rsz_posts_deselected);
//                        post.setScaleType(ImageView.ScaleType.CENTER);
//                        home.startAnimation(zoominanimation);
//                        post.startAnimation(zoomoutanimation);
//                        message.startAnimation(zoomoutanimation);
//                        home.setImageResource(R.drawable.rsz_home_selected);
//                        home.setScaleType(ImageView.ScaleType.CENTER);
//                        message.setImageResource(R.drawable.rsz_messages_deselected);
//                        message.setScaleType(ImageView.ScaleType.CENTER);
//                    }
//                } else if (position == 2) {
//                    if (type.equals("Teacher")) {
//                        post.setImageResource(R.drawable.rsz_posts_deselected);
//                        post.setScaleType(ImageView.ScaleType.CENTER);
//                        message.startAnimation(zoominanimation);
//                        post.startAnimation(zoomoutanimation);
//                        home.startAnimation(zoomoutanimation);
//                        home.setImageResource(R.drawable.ic_insert_invitation_black_24dp);
//                        home.setScaleType(ImageView.ScaleType.CENTER);
//                        message.setImageResource(R.drawable.rsz_messages_selected);
//                        message.setScaleType(ImageView.ScaleType.CENTER);
//                    } else {
//                        post.setImageResource(R.drawable.rsz_posts_deselected);
//                        post.setScaleType(ImageView.ScaleType.CENTER);
//                        message.startAnimation(zoominanimation);
//                        post.startAnimation(zoomoutanimation);
//                        home.startAnimation(zoomoutanimation);
//                        home.setImageResource(R.drawable.rsz_home_unselected);
//                        home.setScaleType(ImageView.ScaleType.CENTER);
//                        message.setImageResource(R.drawable.rsz_messages_selected);
//                        message.setScaleType(ImageView.ScaleType.CENTER);
//                    }
//                }
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });
////
//        viewPager.setCurrentItem(1);
//
//    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
//        adapter.addFragment(new PostFragment(), "POST");

            adapter.addFragment(new DashboardFragment(), "HOME");
        viewPager.setAdapter(adapter);
    }

    private void setUpMenu() {

        // attach to current activity;
        resideMenu = new ResideMenu(this);
        resideMenu.setUse3D(true);
//        resideMenu.setBackground(R.color.colorYellow);
        resideMenu.attachToActivity(this);
        resideMenu.setMenuListener(menuListener);
        //valid scale factor is between 0.0f and 1.0f. leftmenu'width is 150dip.
        resideMenu.setScaleValue(0.6f);
        resideMenu.setBackground(R.color.transparent);


        // create menu items;
//        itemViewProfile = new ResideMenuItem(this, R.mipmap.profile, "PROFILE");
//        itemAddStudent = new ResideMenuItem(this, R.mipmap.add_chat, "ADD STUDENT");
//        itemHelp = new ResideMenuItem(this, R.mipmap.help, "HELP");
//        itemReportProblem = new ResideMenuItem(this, R.drawable.ic_warning_black_24dp, "REPORT A PROBLEM");
        itemLogout = new ResideMenuItem(this, R.mipmap.logout, "LOGOUT");


//        itemViewProfile.setOnClickListener(this);
//        itemAddStudent.setOnClickListener(this);
        itemLogout.setOnClickListener(this);
//        itemHelp.setOnClickListener(this);
//        itemReportProblem.setOnClickListener(this);

//        resideMenu.addMenuItem(itemViewProfile, ResideMenu.DIRECTION_LEFT);
//        resideMenu.addMenuItem(itemAddStudent, ResideMenu.DIRECTION_LEFT);
//        resideMenu.addMenuItem(itemHelp, ResideMenu.DIRECTION_LEFT);
//        resideMenu.addMenuItem(itemReportProblem, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemLogout, ResideMenu.DIRECTION_LEFT);


        // You can disable a direction by setting ->
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_RIGHT);
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_LEFT);

        findViewById(R.id.title_bar_left_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
            }
        });

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return resideMenu.dispatchTouchEvent(ev);
    }

    @Override
    public void onClick(View view) {
//
//    if (view == itemHelp) {
//            Toast.makeText(getApplicationContext(), "WORK IN PROGRESS", Toast.LENGTH_LONG).show();
//        } else if (view == itemReportProblem) {
//            Intent intent = new Intent(getApplicationContext(), ReportProblemActivity.class);
//            startActivity(intent);
//
//        } else
            if (view == itemLogout) {

            Intent intent = new Intent(MainActivity.this,LoginActivity.class);
            //set activity_executed inside insert() method.
            SharedPreferences pref = getSharedPreferences("ActivityPREF", Context.MODE_PRIVATE);
            SharedPreferences.Editor edt = pref.edit();
            edt.putBoolean("activity_executed", false);
            edt.commit();
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
            finish();

            /*Logout User*/
//            if (MyUtils.hasActiveInternetConnection(getApplicationContext())) {
//                Logout();
//            } else {
//                Toast.makeText(getApplicationContext(), "NO INTERNET CONNECTION", Toast.LENGTH_LONG).show();
//            }

        }

        resideMenu.closeMenu();
    }

    private void Logout() {
//Progress Dialog.
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Working.....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        StringRequest stringrequest = new StringRequest(Request.Method.GET, UrlSupport.Logout + id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                        Log.e("logout response", response);
                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        SharedPreferences pref = getSharedPreferences("ActivityPREF", Context.MODE_PRIVATE);
                        SharedPreferences.Editor edt = pref.edit();
                        edt.putBoolean("activity_executed", false);
                        edt.commit();
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                        finish();

                        progressDialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringrequest);


    }

    private void changeFragment(Fragment targetFragment) {
        resideMenu.clearIgnoredViewList();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    // What good method is to access resideMenu？
    public ResideMenu getResideMenu() {
        return resideMenu;
    }

    static class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}

