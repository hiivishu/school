package app.com.shiksha.fragment;


import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.activity.ChatRoomActivity1;
import app.com.shiksha.activity.ChatRoomActivity2;
import app.com.shiksha.activity.ChatRoomActivity3;
import app.com.shiksha.adapter.ChatListingAdapter;
import app.com.shiksha.helper.ConnectivityReceiver;
import app.com.shiksha.helper.UrlSupport;
import app.com.shiksha.model.Chat;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class MessageFragment extends Fragment implements ConnectivityReceiver.ConnectivityReceiverListener{

  private RelativeLayout relativeLayout,relativeLayout1,relativeLayout2;
    SharedPreferences sharedPreferences = null;
    SharedPreferences.Editor editor;
    View view;
    int user_id;
    String type;
    private Button add_room;
    private EditText room_name;
    private ListView listView;
    private ArrayAdapter<String> arrayAdapter;
    private ArrayList<String> list_of_rooms = new ArrayList<>();
    private String name;
    private RecyclerView recyclerView;
    boolean isConnected;
    List<Chat> chat_data ;
    private ChatListingAdapter chatListingAdapter;
    String gurdian_id;
    String room;

//    private DatabaseReference root = FirebaseDatabase.getInstance().getReference("guardians");
    private DatabaseReference root = FirebaseDatabase.getInstance().getReference();




    public MessageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        sharedPreferences = getContext().getSharedPreferences("Login_pref", 0);
        editor = sharedPreferences.edit();
        name = sharedPreferences.getString("name","");
        user_id = sharedPreferences.getInt("id", 0);
        gurdian_id = String.valueOf(user_id);
        type = "Guardian";

        view =  inflater.inflate(R.layout.fragment_message, container, false);
        isConnected = ConnectivityReceiver.isConnected();
        if (isConnected){


        recyclerView = (RecyclerView)view.findViewById(R.id.chat_recyclerview);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

            if (type.equals("Guardian")){
                ChatListingApi();
            }else {
               ChatListingApiTeacher();
            }


        }



        relativeLayout = (RelativeLayout)view.findViewById(R.id.chatuser1);
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ChatRoomActivity1.class);
                startActivity(intent);
            }
        });
        relativeLayout1 = (RelativeLayout)view.findViewById(R.id.chatuser2);
        relativeLayout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ChatRoomActivity2.class);
                startActivity(intent);
            }
        });
        relativeLayout2 = (RelativeLayout)view.findViewById(R.id.chatuser3);
        relativeLayout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ChatRoomActivity3.class);
                startActivity(intent);
            }
        });

        CircleImageView circleImageView = (CircleImageView)view.findViewById(R.id.principle_pic);
        Picasso.with(getContext())
                .load(R.drawable.principle)
                .placeholder(R.drawable.default_avatar) //this is optional the image to display while the url image is downloading
                .into(circleImageView);
        CircleImageView circleImageView1 = (CircleImageView)view.findViewById(R.id.student_pic1);
        Picasso.with(getContext())
                .load(R.drawable.teacher)
                .placeholder(R.drawable.default_avatar) //this is optional the image to display while the url image is downloading
                .into(circleImageView1);
        CircleImageView circleImageView2 = (CircleImageView)view.findViewById(R.id.student_pic2);
        Picasso.with(getContext())
                .load(R.drawable.schooladmin)
                .placeholder(R.drawable.default_avatar) //this is optional the image to display while the url image is downloading
                .into(circleImageView2);

        TextView textView = (TextView)view.findViewById(R.id.person_name);
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Regular.ttf");
        textView.setTypeface(font);
        TextView textView1 = (TextView)view.findViewById(R.id.time_m);
        Typeface font1 = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Regular.ttf");
        textView1.setTypeface(font1);
        TextView textView2 =(TextView)view.findViewById(R.id.messagedetails);
        Typeface font2 = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Regular.ttf");
        textView2.setTypeface(font2);
        TextView textView3 = (TextView)view.findViewById(R.id.degination);
        Typeface font3 = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Regular.ttf");
        textView3.setTypeface(font3);
        TextView textView4 = (TextView)view.findViewById(R.id.person_name1);
        Typeface font4 = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Regular.ttf");
        textView4.setTypeface(font4);
        TextView textView5 = (TextView)view.findViewById(R.id.time_m1);
        Typeface font5 = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Regular.ttf");
        textView5.setTypeface(font5);
        TextView textView6 = (TextView)view.findViewById(R.id.messagedetails1);
        Typeface font6 = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Regular.ttf");
        textView6.setTypeface(font6);
        TextView textView7 = (TextView)view.findViewById(R.id.degination1);
        Typeface font7 = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Regular.ttf");
        textView7.setTypeface(font7);
        TextView textView8 = (TextView)view.findViewById(R.id.person_name2);
        Typeface font8 = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Regular.ttf");
        textView8.setTypeface(font8);
        TextView textView9 = (TextView)view.findViewById(R.id.time_m2);
        Typeface font9 = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Regular.ttf");
        textView9.setTypeface(font9);
        TextView textView10 =(TextView)view.findViewById(R.id.messagedetails2);
        Typeface font10 = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Regular.ttf");
        textView10.setTypeface(font10);
        TextView textView11 = (TextView)view.findViewById(R.id.degination2);
        Typeface font11 = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Regular.ttf");
        textView11.setTypeface(font11);
//        add_room = (Button)view.findViewById(R.id.btnSend);
//        room_name = (EditText)view.findViewById(R.id.txt);
//        listView = (ListView)view.findViewById(R.id.list);
//
//        arrayAdapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1,list_of_rooms);
//        listView.setAdapter(arrayAdapter);
//
//        add_room.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Map<String,Object> map =  new HashMap<String, Object>();
//                map.put(room_name.getText().toString(),"");
//                root.updateChildren(map);
//            }
//        });
//
//        root.child(String.valueOf(user_id)).addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                Log.e("datasnap",dataSnapshot.toString());
////                Chat chat = dataSnapshot.getValue(Chat.class);
////                String addres = chat.getAddress();
////                Log.e("add",addres);
//
//                Set<String> set = new HashSet<String>();
//                Iterator i = dataSnapshot.getChildren().iterator();
//
//                while (i.hasNext()) {
//                    set.add(((DataSnapshot)i.next()).getKey());
//                }
//
//                list_of_rooms.clear();
//                list_of_rooms.addAll(set);
//
//                arrayAdapter.notifyDataSetChanged();
//
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Intent intent = new Intent(getContext(),FirebaseChatTestActivity.class);
//                intent.putExtra("room_name",((TextView)view).getText().toString());
//                intent.putExtra("user_name",name);
//                startActivity(intent);
//            }
//        });

        return view;
    }

    private void ChatListingApiTeacher() {
        StringRequest stringrequest = new StringRequest(Request.Method.GET, UrlSupport.ChatApiListingTeacher + "2",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        chat_data = new ArrayList<>();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.e("message_response",response);
                            String success = jsonObject.getString("success");
                            if (success.equals("true")){
                                JSONObject jsonObj = jsonObject.getJSONObject("result");
                                JSONArray jsonarray = jsonObj.getJSONArray("guardian_id");
                                for (int i = jsonarray.length()-1; i >= 0; i--){
                                    Chat chat = new Chat();
                                    JSONObject json = jsonarray.getJSONObject(i);
                                    int teacher_id = json.getInt("id");
                                    String teachehr_name = json.getString("name");
                                    String teacher_image_url = json.getString("guardian_image_url");
                                    chat.setId(teacher_id);
                                    chat.setName_teacher(teachehr_name);
                                    chat.setTeacher_image_url(teacher_image_url);
                                    chat_data.add(chat);
//                                    room = teacher_id+"&"+gurdian_id;
//                                    Map<String,Object> map =  new HashMap<String, Object>();
//                                    map.put(room,"");
//                                    root.updateChildren(map);
                                }


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        chatListingAdapter = new ChatListingAdapter(getContext(),chat_data);
                        recyclerView.setAdapter(chatListingAdapter);
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {



            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringrequest);

    }

    private void ChatListingApi() {
        StringRequest stringrequest = new StringRequest(Request.Method.GET, UrlSupport.ChatApiListing +"30",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                       chat_data = new ArrayList<>();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            Log.e("message_gurdian",response);
                            String success = jsonObject.getString("success");
                            if (success.equals("true")){
                                JSONObject jsonObj = jsonObject.getJSONObject("result");
                                JSONArray jsonarray = jsonObj.getJSONArray("teacher_id");
                                for (int i = 0; i<jsonarray.length();i++){
                                    Chat chat = new Chat();
                                    JSONObject json = jsonarray.getJSONObject(i);
                                    int teacher_id = json.getInt("id");
                                    String teachehr_name = json.getString("name");
                                    String teacher_image_url = json.getString("teacher_image_url");
                                    chat.setId(teacher_id);
                                    chat.setName_teacher(teachehr_name);
                                    chat.setTeacher_image_url(teacher_image_url);
                                    chat_data.add(chat);

                                }


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        chatListingAdapter = new ChatListingAdapter(getContext(),chat_data);
                        recyclerView.setAdapter(chatListingAdapter);

                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {



            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringrequest);



    }



    @Override
    public void onResume() {
        super.onResume();
        if (chatListingAdapter!=null){
            recyclerView.setAdapter(chatListingAdapter);
        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
//
//    @Override
//    public void fragmentBecameVisible() {
//
//    }
}
