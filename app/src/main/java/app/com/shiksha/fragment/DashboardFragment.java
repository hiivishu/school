package app.com.shiksha.fragment;


import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.lzyzsd.circleprogress.ArcProgress;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.com.shiksha.R;
import app.com.shiksha.activity.CalenderActivity;
import app.com.shiksha.activity.EventsActivity;
import app.com.shiksha.activity.FeeActivity;
import app.com.shiksha.activity.GalleryActivity;
import app.com.shiksha.activity.NewAttendanceActivity;
import app.com.shiksha.activity.NewNoticeActivity;
import app.com.shiksha.activity.ResultActivity;
import app.com.shiksha.activity.ShopActivity;
import app.com.shiksha.activity.StudyMaterialActivity;
import app.com.shiksha.activity.SyllabusActivity;
import app.com.shiksha.activity.TaskActivity;
import app.com.shiksha.activity.TimeTableActivity;
import app.com.shiksha.adapter.CustomGridHome;
import app.com.shiksha.helper.CircleDisplay;
import app.com.shiksha.helper.UrlSupport;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment implements AdapterView.OnItemSelectedListener, CircleDisplay.SelectionListener {
    GridView grid;
    String[] web = {
            "Attendance",
            "Results",
//            "Fee Details",
//            "Study Material",
            "Timetable",
            "Homework",
            "Calender",
            "Gallery",
            "Events",
            "Syllabus",
            "ClassMate"


    };
    int[] imageId = {
            R.mipmap.attendance,
            R.mipmap.results,
//            R.mipmap.fee,
//            R.mipmap.study_material,
            R.mipmap.timetable,
            R.mipmap.homework,
            R.mipmap.calendar,
            R.mipmap.gallery,
            R.mipmap.events,
            R.mipmap.syllabus,
            R.drawable.classma


    };
    SharedPreferences sharedPreferences = null;
    SharedPreferences.Editor editor;
    int user_id;
    String gurdian_id;
    String student_id;
    String ClassName,ClassId,Section;
    String atte;
    private CircleDisplay mCircleDisplay;
    private ArcProgress arcProgress;
    private Spinner spinner;
    private ArrayList<String> student;
    private JSONArray student_data;
    private JSONObject student_data_new;
    private TextView textView, textView1;
    String attendance = "";
    private String email,password;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgressDialog progressDialog;
    private RelativeLayout new_notice_lay,new_event_lay;
    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        //Initializing the ArrayList
        sharedPreferences = getContext().getSharedPreferences("Login_pref", 0);
        editor = sharedPreferences.edit();

        email = sharedPreferences.getString("email","");
        password = sharedPreferences.getString("password","");
        user_id = sharedPreferences.getInt("id", 0);


        gurdian_id = String.valueOf(user_id);
        student = new ArrayList<String>();
        // Spinner element
        spinner = (Spinner) view.findViewById(R.id.spinner2);


        // Spinner click listener
        spinner.setOnItemSelectedListener(this);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout_dashboard);


        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
//        swipeRefreshLayout.post(new Runnable() {
//            @Override
//            public void run() {
//                swipeRefreshLayout.setRefreshing(true);
//                getData();
//
//            }
//        });
swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }
});
        getData();
//        getData();
        mCircleDisplay = (CircleDisplay) view.findViewById(R.id.circleDisplay);


        mCircleDisplay.setAnimDuration(2000);
        mCircleDisplay.setValueWidthPercent(15f);
        mCircleDisplay.setFormatDigits(1);
        mCircleDisplay.setDimAlpha(80);
        mCircleDisplay.setStartAngle(130);
        mCircleDisplay.setSelectionListener(this);
        mCircleDisplay.setTouchEnabled(true);
        mCircleDisplay.setUnit("%");
        mCircleDisplay.setStepSize(0.5f);
        mCircleDisplay.showValue(65f,100f,true);


        CustomGridHome adapter = new CustomGridHome(getActivity(), web, imageId);
        grid = (GridView) view.findViewById(R.id.grid);
        grid.setAdapter(adapter);
        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                if (position == 0) {
                    Intent intent = new Intent(getContext(), NewAttendanceActivity.class);
                    intent.putExtra("student_id", student_id);
                    intent.putExtra("attentancevaalue",attendance);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

                } else if (position == 1) {
                    Intent intent = new Intent(getContext(), ResultActivity.class);
                    intent.putExtra("student_id", student_id);
                    intent.putExtra("ClassId",ClassId);
                    intent.putExtra("ClassName",ClassName);
                    intent.putExtra("Section",Section);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                }
//                else if (position == 2) {
//                    Intent intent = new Intent(getContext(), FeeActivity.class);
//                    intent.putExtra("student_id", student_id);
//                    startActivity(intent);
//                    getActivity().overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
//                } else if (position == 3) {
//                    Intent intent = new Intent(getContext(), StudyMaterialActivity.class);
//                    intent.putExtra("student_id", student_id);
//                    startActivity(intent);
//                    getActivity().overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
//                }
                else if (position == 2) {
                    Intent intent = new Intent(getContext(), TimeTableActivity.class);
                    intent.putExtra("student_id", student_id);
                    intent.putExtra("ClassId",ClassId);
                    intent.putExtra("ClassName",ClassName);
                    intent.putExtra("Section",Section);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                } else if (position == 3) {
                    Intent intent = new Intent(getContext(), TaskActivity.class);
                    intent.putExtra("student_id", student_id);
                    intent.putExtra("ClassId",ClassId);
                    intent.putExtra("ClassName",ClassName);
                    intent.putExtra("Section",Section);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                } else if (position == 4) {
                    Intent intent = new Intent(getContext(), CalenderActivity.class);
                    intent.putExtra("student_id", student_id);
                    intent.putExtra("ClassId",ClassId);
                    intent.putExtra("ClassName",ClassName);
                    intent.putExtra("Section",Section);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                } else if (position == 5) {
                    Intent intent = new Intent(getContext(), GalleryActivity.class);
                    intent.putExtra("student_id", student_id);
                    intent.putExtra("ClassId",ClassId);
                    intent.putExtra("ClassName",ClassName);
                    intent.putExtra("Section",Section);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                } else if (position == 6) {
                    Intent intent = new Intent(getContext(), EventsActivity.class);
                    intent.putExtra("gurdian_id", gurdian_id);
                    intent.putExtra("ClassId",ClassId);
                    intent.putExtra("ClassName",ClassName);
                    intent.putExtra("Section",Section);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                } else if (position == 7) {
                    Intent intent = new Intent(getContext(), SyllabusActivity.class);
                    intent.putExtra("student_id", student_id);
                    intent.putExtra("ClassId",ClassId);
                    intent.putExtra("ClassName",ClassName);
                    intent.putExtra("Section",Section);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                }else if (position ==8){
                    Intent intent = new Intent(getContext(), ShopActivity.class);
                    intent.putExtra("student_id", student_id);
                    intent.putExtra("ClassId",ClassId);
                    intent.putExtra("ClassName",ClassName);
                    intent.putExtra("Section",Section);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

                }
//                }else if (position==10){
//                    Intent intent = new Intent(getContext(), ShopActivity.class);
//                    intent.putExtra("student_id", student_id);
//                    startActivity(intent);
//                    getActivity().overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
//                }

            }
        });

        new_event_lay = (RelativeLayout)view.findViewById(R.id.new_event_lay);
        new_event_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), EventsActivity.class);
                intent.putExtra("gurdian_id", gurdian_id);
                startActivity(intent);
            }
        });
        new_notice_lay = (RelativeLayout)view.findViewById(R.id.new_notice_lay);
        new_notice_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), NewNoticeActivity.class);
                startActivity(intent);
            }
        });
        textView = (TextView) view.findViewById(R.id.textnum);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), NewNoticeActivity.class);
                startActivity(intent);
            }
        });
        textView1 = (TextView) view.findViewById(R.id.eventnum);
        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), EventsActivity.class);
                intent.putExtra("gurdian_id", gurdian_id);
                startActivity(intent);
            }
        });
        TextView textView2 = (TextView) view.findViewById(R.id.newnotice);
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Lato-Regular.ttf");
        textView2.setTypeface(font);
        TextView textView3 = (TextView) view.findViewById(R.id.eventtext);
        Typeface font1 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Lato-Regular.ttf");
        textView3.setTypeface(font1);
        TextView textView4 = (TextView) view.findViewById(R.id.attendancetext);
        Typeface font2 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Lato-Regular.ttf");
        textView4.setTypeface(font2);

        return view;
    }



    private void getData() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Working.....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        final StringRequest stringRequest = new StringRequest(Request.Method.GET, UrlSupport.Login+"UserId="+email+"+&Password="+password+"",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("LoginResponse", "" + response);
                        try {
                            JSONObject jsonobj = new JSONObject(response);
                            JSONObject jsonObject = jsonobj.getJSONObject("Login");
                            JSONObject jsonObject1 = jsonObject.getJSONObject("Status");
                            String sucess = jsonObject1.getString("Type");
                            if (sucess.equals("success")) {
                                JSONObject jsonObject2 = jsonObject.getJSONObject("Data");
//                                student_data = jsonobj.getJSONArray("data");
                                student_data_new = jsonObject2;

                                getStudent(student_data_new);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
//                        Log.e("studentnew1",""+student_name+stu_id);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        progressDialog.dismiss();

                    }
                }) {

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);

//        StringRequest stringRequest = new StringRequest(Request.Method.GET, UrlSupport.Dashboard_data +"30",
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        Log.e("DashboardResponse", "" + response);
//
//                        try {
//                            JSONObject jsonObject = new JSONObject(response);
//                            String success = jsonObject.getString("success");
//                            JSONObject jsonObject1 = jsonObject.getJSONObject("result");
//                            student_data = jsonObject1.getJSONArray("student_data");
//                            Log.e("result", student_data.toString());
////                            student_data = jsonObject1.getJSONArray("student_data");
//                            getStudent(student_data);
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        swipeRefreshLayout.setRefreshing(false);
//
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        swipeRefreshLayout.setRefreshing(false);
//                        Toast.makeText(getContext(), "Something Went Wrong Please Try Again", Toast.LENGTH_LONG).show();
//
//                    }
//                }) {
//        };
//
//        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
//        requestQueue.add(stringRequest);

    }

    private void getStudent(JSONObject student_data_new) {

//        for (int i = 0; i < student_data.length(); i++) {
            try {
//                JSONObject json = student_data.getJSONObject(i);

                String studentname = student_data_new.getString("StudentName");


                   student.add(studentname);

                Log.e("stu", studentname.toString());
//                student.add(json.getString("name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }


        spinner.setAdapter(new ArrayAdapter<String>(getContext(), R.layout.spinnertext, student));


    }

    private String getStudentId(int position) {
        String id = null;
        try {
            String studentID = student_data_new.getString("StudentId");
            id = studentID;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return String.valueOf(id);
    }

    private String getdob(int position) {
        String dob = "";
        try {
            //JSONObject jsonObject3 = student_data.getJSONObject(position);

            String dobN = student_data_new.getString("DOB");
            dob = dobN;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return dob;
    }

    private String getClassName(int position) {
        String ClassName = "";
        try {
            //JSONObject jsonObject3 = student_data.getJSONObject(position);

            String ClassNameN = student_data_new.getString("ClassName");
            ClassName = ClassNameN;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ClassName;
    }

    private String getClassId(int position) {
        String ClassId = "";
        try {
            //JSONObject jsonObject3 = student_data.getJSONObject(position);

            String ClassIdN = student_data_new.getString("ClassId");
            ClassId = ClassIdN;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return ClassId;
    }

    private String getSection(int position) {
        String Section = "";
        try {
            //JSONObject jsonObject3 = student_data.getJSONObject(position);

            String SectionN = student_data_new.getString("Section");
            Section = SectionN;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return Section;
    }



//    private String getAttendance(int position) {
//
//        try {
//            JSONObject jsonObject = student_data.getJSONObject(position);
//            attendance = jsonObject.getString("attendance");
//            Log.e("attendance", "" + attendance);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        return attendance;
//    }
//
//    private String getNewNotice(int position) {
//        int newnotice = 1;
//        try {
//            JSONObject jsonObject = student_data.getJSONObject(position);
//
//            newnotice = jsonObject.getInt("notice");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        return String.valueOf(newnotice);
//    }
//
//    private String getNewEvents(int position) {
//        int newevents = 3;
//
//        try {
//            JSONObject jsonObject = student_data.getJSONObject(position);
//
//            newevents = jsonObject.getInt("event");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        return String.valueOf(newevents);
//    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//        textView.setText(getNewNotice(position));
//        mCircleDisplay.showValue(Float.parseFloat(getAttendance(position)), 100f, true);
//        textView1.setText(getNewEvents(position));
        student_id = getStudentId(position);
        ClassName = getClassName(position);
        ClassId = getClassId(position);
        Section = getSection(position);
//        atte = getAttendance(position);


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {


    }

    @Override
    public void onSelectionUpdate(float val, float maxval) {

    }

    @Override
    public void onValueSelected(float val, float maxval) {

    }

//    @Override
//    public void fragmentBecameVisible() {
//
//    }


}
