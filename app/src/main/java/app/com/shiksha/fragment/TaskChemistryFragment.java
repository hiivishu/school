package app.com.shiksha.fragment;



import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import app.com.shiksha.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TaskChemistryFragment extends DialogFragment {

    private TextView textView;

    public TaskChemistryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_task_chemistry, container, false);

        textView = (TextView)view.findViewById(R.id.close_frag);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return view;
    }

}
