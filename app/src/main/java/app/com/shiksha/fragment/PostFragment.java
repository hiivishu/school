package app.com.shiksha.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.com.shiksha.AppController;
import app.com.shiksha.R;
import app.com.shiksha.adapter.PostAdapter;
import app.com.shiksha.helper.ConnectivityReceiver;
import app.com.shiksha.helper.EndlessRecyclerViewScrollListener;
import app.com.shiksha.helper.UrlSupport;
import app.com.shiksha.model.Post;

/**
 * A simple {@link Fragment} subclass.
 */
public class PostFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, ConnectivityReceiver.ConnectivityReceiverListener {

    SharedPreferences sharedPreferences = null;
    SharedPreferences.Editor editor;
    int user_id;
    String gurdian_id;
    boolean isConnected;
    private RecyclerView recyclerView;
    private PostAdapter postAdapter;
    private List<Post> posts_data;
    private LinearLayout  no_internet_lay;
    private SwipeRefreshLayout swipeRefreshLayout;
    private boolean _hasLoadedOnce = false;

    private EndlessRecyclerViewScrollListener scrollListener;

    public PostFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        sharedPreferences = getContext().getSharedPreferences("Login_pref", 0);
        editor = sharedPreferences.edit();
        user_id = sharedPreferences.getInt("id", 0);

        final View view = inflater.inflate(R.layout.fragment_post, container, false);
//        getActivity().setProgressBarVisibility(true);
//        sharedPreferences = getContext().getSharedPreferences("Login_pref", 1);
//        editor = sharedPreferences.edit();
//        user_id = sharedPreferences.getInt("id", 0);
        no_internet_lay = (LinearLayout) view.findViewById(R.id.no_Internet_lay);
        gurdian_id = String.valueOf(user_id);
        // Manually checking internet connection
        isConnected = ConnectivityReceiver.isConnected();

        if (isConnected) {

            recyclerView = (RecyclerView) view.findViewById(R.id.recycleview_post);
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
            recyclerView.setLayoutManager(layoutManager);


        /*Request Post API*/

            PostApi();

        } else {

            no_internet_lay.setVisibility(View.VISIBLE);
        }
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);

        swipeRefreshLayout.setOnRefreshListener(this);

        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(true);

                    PostApi();
            }
        });


        return view;
    }





    public void PostApi() {


        StringRequest stringrequest = new StringRequest(Request.Method.GET, UrlSupport.POST_URL + gurdian_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                            posts_data = new ArrayList<>();
                            try {
                                JSONObject jsonobj1 = new JSONObject(response);
                                Log.e("jsonfd", "" + jsonobj1.toString());

                                String success = jsonobj1.getString("success");
                                Log.e("jsonfd1", "" + success);
                                if (success.equals("true")) {
                                    JSONArray jsonarray1 = jsonobj1.getJSONArray("result");
                                    Log.e("jsonfd2", "" + jsonarray1);

                                    /* for (int i = jsonarray1.length()-1; i >= 0; i--) {*/
                                    for (int i = 0; i < jsonarray1.length(); i++) {
                                        Post post1 = new Post();
                                        JSONObject jsonobj2 = jsonarray1.getJSONObject(i);
                                        int id = jsonobj2.getInt("id");
                                        String title = jsonobj2.getString("title");
                                        String postdata = jsonobj2.getString("post");
                                        int share = jsonobj2.getInt("share");
                                        int like = jsonobj2.getInt("like");
                                        int comment = jsonobj2.getInt("comment");
                                        String created_at = jsonobj2.getString("created_at");
                                        String post_image_url = jsonobj2.getString("post_image_url");
                                        String is_liked = jsonobj2.getString("is_liked");

                                        Log.e("post_img_url",post_image_url);
                                        post1.setCreated_at(created_at);
                                        post1.setId(id);
                                        post1.setLike(like);
                                        post1.setShare(share);
                                        post1.setPost(postdata);
                                        post1.setTitle(title);
                                        post1.setComment(comment);
                                        post1.setPost_image_url(post_image_url);
                                        post1.setIs_liked(is_liked);

                                        posts_data.add(post1);
//
                                    }

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            postAdapter = new PostAdapter(getContext(), posts_data);
                            recyclerView.setAdapter(postAdapter);

                            swipeRefreshLayout.setRefreshing(false);


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                swipeRefreshLayout.setRefreshing(false);

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringrequest);

    }

    @Override
    public void onResume() {
        super.onResume();
        PostApi();

        if (postAdapter != null)
            recyclerView.setAdapter(postAdapter);

        // register connection status listener
        AppController.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onRefresh() {
        if (isConnected) {
            PostApi();
        } else {
            swipeRefreshLayout.setRefreshing(false);
        }

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

        no_internet_lay.setVisibility(View.GONE);
        PostApi();
    }

}
