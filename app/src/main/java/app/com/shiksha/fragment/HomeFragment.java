package app.com.shiksha.fragment;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import app.com.shiksha.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    private TabLayout tabLayout;
    public static ViewPager viewPager = null;
    ViewPagerAdapter adapter;
    SharedPreferences sharedPreferences = null;
    SharedPreferences.Editor editor;
    String type;
    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        sharedPreferences = getContext().getSharedPreferences("Login_pref", 0);
        editor = sharedPreferences.edit();
        type = sharedPreferences.getString("type","");
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                viewPager.setCurrentItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
//        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int i, float v, int i1) {
//
//            }
//
//            @Override
//            public void onPageSelected(int position) {
//                viewPager.setCurrentItem(position);
//            }
//
//
//            @Override
//            public void onPageScrollStateChanged(int i) {
//
//            }
//
//        });
        viewPager.setCurrentItem(1);
        return view;

    }

     /*--------View Pager-------------start*/

    private void setupViewPager(ViewPager viewPager) {
       adapter = new ViewPagerAdapter(getFragmentManager());
//        adapter.addFragment(new PostFragment(), "POST");
        adapter.addFragment(new DashboardFragment(), "HOME");
//        adapter.addFragment(new MessageFragment(), "MESSAGE");
        viewPager.setAdapter(adapter);
    }

    //        if (type.equals("guardian")){
//
//        } else if (type.equals("teacher")){
//            adapter.addFragment(new MarkAttendanceFragment(),"ATTENDANCE");
//        }

   static class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}
