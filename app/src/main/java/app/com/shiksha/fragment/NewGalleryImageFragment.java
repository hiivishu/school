package app.com.shiksha.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.adapter.GalleryAdapter;
import app.com.shiksha.helper.UrlSupport;
import app.com.shiksha.model.Gallery;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewGalleryImageFragment extends Fragment {


    private RecyclerView recyclerView;
    public NewGalleryImageFragment() {
        // Required empty public constructor
    }
    private GalleryAdapter galleryAdapter;
    private List<Gallery> gallery_data;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_new_gallery_image, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.gnlay);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        GalleryApi();
        return view;
    }

    private void GalleryApi() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, UrlSupport.GalleryImgApi,
                new Response.Listener<String>() {


                    @Override
                    public void onResponse(String response) {
                        Log.e("GalleryResponse", response);

                        gallery_data = new ArrayList<>();


                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String success = jsonObject.getString("success");

                            if (success.equals("true")){
                                JSONArray jsonArray = jsonObject.getJSONArray("result");

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    Gallery gallery = new Gallery();

                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                                    int id = jsonObject1.getInt("id");
                                    String title = jsonObject1.getString("title");
                                    String description = jsonObject1.getString("description");
                                    String created_at = jsonObject1.getString("created_at");
                                    String gallery_image_url = jsonObject1.getString("gallery_image_url");

                                    JSONArray jsonArray2 = jsonObject1.getJSONArray("gallery_assets");
                                    for (int j = 0; j < jsonArray2.length(); j++) {
                                        JSONObject jsonObj = jsonArray2.getJSONObject(j);
                                        String gallery_asset_url = jsonObj.getString("gallery_asset_url");

                                            gallery.setGallery_asset_url(gallery_asset_url);

                                    }

                                    gallery.setId(id);
                                    gallery.setTitle(title);
                                    gallery.setDescription(description);
                                    gallery.setCreated_at(created_at);
                                    gallery.setGallery_image_url(gallery_image_url);


//                                    ImageView image = new ImageView(getContext());
//                                    image.setLayoutParams(new android.view.ViewGroup.LayoutParams(20,20));
//                                    image.setPadding(5,0,5,0);
//                                    image.setImageResource(R.drawable.circle_dot_gallery);
//
//                                    ImageView image1 = new ImageView(getContext());
//                                    image1.setLayoutParams(new android.view.ViewGroup.LayoutParams(20,20));
//                                    image1.setPadding(5,0,5,0);
//                                    image1.setImageResource(R.drawable.circle_dot_gallery);
//
//
//                                    // Adds the view to the layout
//                                    linearlay.addView(image);
//                                    linearlay1.addView(image1);
                                    gallery_data.add(gallery);



                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        galleryAdapter = new GalleryAdapter(getContext(),gallery_data);
                       recyclerView.setAdapter(galleryAdapter);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);


    }

}
