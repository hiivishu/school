package app.com.shiksha.fragment;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.adapter.StudentListingTeacherAdapter;
import app.com.shiksha.helper.ConnectivityReceiver;
import app.com.shiksha.helper.UrlSupport;
import app.com.shiksha.model.StudentListing;
import android.app.AlertDialog;
import android.content.DialogInterface;

/**
 * A simple {@link Fragment} subclass.
 */
public class MarkAttendanceFragment extends Fragment implements AdapterView.OnItemSelectedListener,ConnectivityReceiver.ConnectivityReceiverListener {


    private RecyclerView recyclerView;
    private StudentListingTeacherAdapter studentListingTeacherAdapter;
    private List<StudentListing> studentListings_data;
    boolean isConnected;
    private FloatingActionButton floatingActionButton;
    public MarkAttendanceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_mark_attendance, container, false);

        isConnected = ConnectivityReceiver.isConnected();
        if (isConnected){
            recyclerView = (RecyclerView)view.findViewById(R.id.attendance_data);
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
            recyclerView.setLayoutManager(layoutManager);
            StudentListingApi();
            floatingActionButton = (FloatingActionButton)view.findViewById(R.id.fab);
            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            getContext());
                    builder.setTitle("SUBMIT ATTENDANCE");
                    builder.setMessage("Total Present");
                    builder.setNegativeButton("NO",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int which) {
//                                    Toast.makeText(getContext(),"No is clicked",Toast.LENGTH_LONG).show();
                                }
                            });
                    builder.setPositiveButton("YES",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int which) {
//                                    Toast.makeText(getContext(),"Yes is clicked",Toast.LENGTH_LONG).show();
                                }
                            });
                    builder.show();

                }
            });


        }

       // Spinner element
        Spinner spinner = (Spinner) view.findViewById(R.id.spinner_date);
        Spinner spinner1 = (Spinner) view.findViewById(R.id.spinner_class);
        // Spinner click listener
        spinner.setOnItemSelectedListener(this);
        spinner1.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> date = new ArrayList<String>();
        date.add("15-Feb-2017");
        date.add("16-Feb-2017");
        date.add("17-Feb-2017");
        date.add("19-Feb-2017");
        date.add("23-Feb-2017");
        date.add("27-Feb-2017");
        // Spinner Drop down elements
        List<String> class_data = new ArrayList<String>();
        class_data.add("9 A");
        class_data.add("9 B");
        class_data.add("10 C");
        class_data.add("10 A");
        class_data.add("12 A");
        class_data.add("12 B");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_attendance, date);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(getContext(), R.layout.spinner_attendance, class_data);

        // Drop down layout style - list view with radio button
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner1.setAdapter(dataAdapter1);

        return view;
    }

    private void StudentListingApi() {

        StringRequest stringrequest = new StringRequest(Request.Method.GET, UrlSupport.Student_Listing_for_attendance_api ,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        studentListings_data = new ArrayList<>();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            if (success.equals("true")){
                                JSONArray jsonarray = jsonObject.getJSONArray("result");
                             for (int i =0; i<jsonarray.length();i++){
                                 StudentListing studentListing = new StudentListing();
                                 JSONObject json = jsonarray.getJSONObject(i);
                                 String name = json.getString("name");
                                 studentListing.setStudent_name(name);
                                 studentListings_data.add(studentListing);

                             }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        studentListingTeacherAdapter = new StudentListingTeacherAdapter(getContext(),studentListings_data);
                        recyclerView.setAdapter(studentListingTeacherAdapter);
//

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {



            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringrequest);
    }

    @Override
    public void onItemSelected(AdapterView parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();

        // Showing selected spinner item


    }

    public void onNothingSelected(AdapterView arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}


