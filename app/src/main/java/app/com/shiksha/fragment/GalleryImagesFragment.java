package app.com.shiksha.fragment;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.adapter.GalleryAdapter;
import app.com.shiksha.helper.ConnectivityReceiver;
import app.com.shiksha.model.Gallery;


/**
 * A simple {@link Fragment} subclass.
 */
public class GalleryImagesFragment extends Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {


    boolean isConnected;
    private RecyclerView  gallery_rv;
    private GalleryAdapter galleryAdapter;
    private List<Gallery> gallery_data;
    private ProgressDialog progressDialog;
    public GalleryImagesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_gallery_images, container, false);

        isConnected = ConnectivityReceiver.isConnected();


        if (isConnected) {

            GalleryApi();

            gallery_rv = (RecyclerView) view.findViewById(R.id.gallery_rv);
            gallery_rv.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
            gallery_rv.setLayoutManager(layoutManager);
        }

        return view;
    }


    private void GalleryApi() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Working.....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        String url = "http://ccclucknow.com/ccclucknow/WebService.asmx/GetGallery";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("GalleryResponse", response);

                        gallery_data = new ArrayList<>();

                        try {
                            JSONObject jsonObject =  new JSONObject(response);
                            JSONObject jsonObject1 = jsonObject.getJSONObject("Gallery");

                            JSONArray jsonArray = jsonObject1.getJSONArray("Data");

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject3 = jsonArray.getJSONObject(i);
                                    Gallery gallery = new Gallery();
                                    String gallery_image_url = jsonObject3.getString("ImagePath");
                                     String gallery_image_url1 = gallery_image_url.replaceAll("~", "");
                                    Log.e("imageUr",gallery_image_url1);
                                    int Id = jsonObject3.getInt("Id");
                                    String Title = jsonObject3.getString("Title");
                                    String Description = jsonObject3.getString("Description");
                                    gallery.setTitle(Title);
                                    gallery.setDescription(Description);
                                    gallery.setId(Id);
                                    gallery.setGallery_image_url(gallery_image_url1);
                                    gallery_data.add(gallery);

                                }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        galleryAdapter = new GalleryAdapter(getContext(),gallery_data);
                        gallery_rv.setAdapter(galleryAdapter);
                        galleryAdapter.notifyDataSetChanged();
                        progressDialog.dismiss();


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                   progressDialog.dismiss();
                    }
                }) {

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);



    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}
