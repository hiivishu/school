package app.com.shiksha.helper;

/**
 * Created by vishal on 11/23/16.
 */

public class ChatMessage {

    public boolean left;
    public String message;

    public ChatMessage(boolean left,String message){
        super();
        this.left = left;
        this.message = message;
    }
}
