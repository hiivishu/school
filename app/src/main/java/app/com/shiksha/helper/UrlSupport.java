package app.com.shiksha.helper;

/**
 * Created by devion on 4/11/16.
 */

public class UrlSupport {

    public final static String AllLoginApi = "http://54.197.178.15:8000/api/login";
    public final static String BASE_URL = "http://54.197.178.15:8000/";

    public final static String SendOtp = "http://54.197.178.15:8000/api/guardian/send_otp";

    public final static String Login ="http://ccclucknow.com/ccclucknow/WebService.asmx/Login?";

    public final static String POST_URL = "http://54.197.178.15:8000/api/posts?guardian_id=";

    public final static String VerifyOtp = "http://54.197.178.15:8000/api/guardian/contact_verify";

    public final static String Logout = "http://54.197.178.15:8000/api/guardian/logout?";

    public final static String Like = "http://54.197.178.15:8000/api/likes?";

    public final static String Share = "http://54.197.178.15:8000/api/shares?";

    public final static String Comment_Listing = "http://54.197.178.15:8000/api/comment_listing?post_id=";

    public final static String Comment_posting = "http://54.197.178.15:8000/api/post_comments";

    public final static String Dashboard_data = "http://54.197.178.15:8000/api/dashboard?guardian_id=";

    public final static String Attendance_api = "http://ccclucknow.com/WebService.asmx?op=GetAttendance?";

    public final static String Feedetails = "http://54.197.178.15:8000/api/student_fee_details?student_id=";

    public final static String TimeTableApi = "http://ccclucknow.com/ccclucknow/WebService.asmx/GetTimeTable?";

    public final static String ResultApi = "http://54.197.178.15:8000/api/student_result?student_id=";

    public final static String SyllabusApi = "http://ccclucknow.com/ccclucknow/WebService.asmx/GetSyllabus?";

    public final static String EventsApi = "http://ccclucknow.com/ccclucknow/WebService.asmx/GetEvents";

    public final static String EventAcceptApi = "http://54.197.178.15:8000/api/event_invitation?guardian_id=";

    public final static String HomeWorkApi = "http://ccclucknow.com/ccclucknow/WebService.asmx/GetAssignment?";

    public final static String ReportAProblem = "http://54.197.178.15:8000/api/report_problems?guardian_id=";

    public final static String NewNoticesApi = "http://54.197.178.15:8000/api/notices";

    public final static String ForgotPasswordApi = "http://54.197.178.15:8000/api/guardians/passwordreset?email=";

    public final static String StudyMaterialApi = "http://54.197.178.15:8000/api/study_material?student_id=";

    public final static String UpdateProfileApi = "http://54.197.178.15:8000/api/guardian/updateprofile/";

    public final static String GalleryImgApi = "http://54.197.178.15:8000/api/gallery_listing";

    public final static String ChatApiListing = "http://54.197.178.15:8000/api/chatid_guardian?guardian_id=";

    public final static String ChatApiListingTeacher = "http://54.197.178.15:8000/api/chatid_teacher?teacher_id=";

    public final static String HolidayListApi = "http://koshishindia.co.in/StudentClassWebService/EventClassService.asmx/GetHoliday";

    public final static String Student_Listing_for_attendance_api = "http://54.197.178.15:8000/api/student_list?section_id=1";

    public final static String ClassMateAPi = "http://ccclucknow.com/ccclucknow/WebService.asmx/Classmate?";

}
