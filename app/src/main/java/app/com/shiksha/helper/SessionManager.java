package app.com.shiksha.helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by devion on 4/11/16.
 */

public class SessionManager {

    //Logcat Log
    private static String TAG = SessionManager.class.getSimpleName();

    //Shared Prefrence

    SharedPreferences sharedPreferences;

    SharedPreferences.Editor editor;

    Context _context;

    int PRIVATE_MODE = 0 ;

    private static final String PREF_NAME = "SchulerzLogin";

    private static final String KEY_IS_LOGGEDIN = "isLoggedIn";


    public SessionManager(Context context){

        this._context = context;
        sharedPreferences = _context.getSharedPreferences(PREF_NAME,PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public void setLogin(boolean isLoggedIn){

    }

    public boolean isLoggedIn(){
        return sharedPreferences.getBoolean(KEY_IS_LOGGEDIN,false);
    }

}
