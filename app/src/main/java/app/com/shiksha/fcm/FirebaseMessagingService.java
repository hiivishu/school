//package app.com.shiksha.fcm;
//
//import android.app.Notification;
//import android.app.NotificationManager;
//import android.content.Context;
//import android.support.v4.app.NotificationCompat;
//import android.util.Log;
//
//import com.google.firebase.messaging.RemoteMessage;
//
//import app.com.shiksha.R;
//
///**
// * Created by vishal on 1/27/17.
// */
//
//public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {
//
//
//    @Override
//    public void onMessageReceived(RemoteMessage remoteMessage) {
//        super.onMessageReceived(remoteMessage);
//
//        Log.e("Notificationdata", "From: " + remoteMessage.getFrom());
//        Log.e("Notificationdata_body", "Notification Message Body: " + remoteMessage.getNotification().getBody());
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
//        notificationBuilder.setSmallIcon(R.mipmap.app_icon);
//        notificationBuilder.setAutoCancel(true);
//        notificationBuilder.setDefaults(Notification.DEFAULT_ALL);
//        notificationBuilder.setContentTitle("SCHULERZ");
//        notificationBuilder.setContentText(remoteMessage.getNotification().getBody());
//
////        Intent notificationIntent = new Intent(this, MainActivity.class);
////        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent,
////                PendingIntent.FLAG_UPDATE_CURRENT);
////        notificationBuilder.setContentIntent(contentIntent);
//
//        // Add as notification
//        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        manager.notify(0, notificationBuilder.build());
//    }
//}
