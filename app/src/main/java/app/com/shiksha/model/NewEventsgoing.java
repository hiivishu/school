package app.com.shiksha.model;

/**
 * Created by vishal on 12/5/16.
 */

public class NewEventsgoing {


    private int goingid;
    private String EventName;
    private String EventDate;
    private String EventPlace;
    private String EventTime;
    private String Description;
    private String FileUpload;


    public String getEventName() {
        return EventName;
    }

    public void setEventName(String eventName) {
        EventName = eventName;
    }

    public String getEventDate() {
        return EventDate;
    }

    public void setEventDate(String eventDate) {
        EventDate = eventDate;
    }

    public String getEventPlace() {
        return EventPlace;
    }

    public void setEventPlace(String eventPlace) {
        EventPlace = eventPlace;
    }

    public String getEventTime() {
        return EventTime;
    }

    public void setEventTime(String eventTime) {
        EventTime = eventTime;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getFileUpload() {
        return FileUpload;
    }

    public void setFileUpload(String fileUpload) {
        FileUpload = fileUpload;
    }
}
