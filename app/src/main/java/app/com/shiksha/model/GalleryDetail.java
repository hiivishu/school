package app.com.shiksha.model;

/**
 * Created by Vishu on 25/10/17.
 */

public class GalleryDetail {

    private String Image;
    private String Date;

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }
}
