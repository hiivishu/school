package app.com.shiksha.model;

/**
 * Created by vishal on 12/26/16.
 */

public class Gallery {

    private int id ;

    private String title;

    private String description;

    private String created_at ;

    private String gallery_image_url;

    private String gallery_asset_url;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getGallery_image_url() {
        return gallery_image_url;
    }

    public void setGallery_image_url(String gallery_image_url) {
        this.gallery_image_url = gallery_image_url;
    }

    public String getGallery_asset_url() {
        return gallery_asset_url;
    }

    public void setGallery_asset_url(String gallery_asset_url) {
        this.gallery_asset_url = gallery_asset_url;
    }
}
