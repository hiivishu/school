package app.com.shiksha.model;

/**
 * Created by Vishu on 30/09/17.
 */

public class Syllabus {

    private String ClassId;
    private String ContentFile;

    public String getClassId() {
        return ClassId;
    }

    public void setClassId(String classId) {
        ClassId = classId;
    }

    public String getContentFile() {
        return ContentFile;
    }

    public void setContentFile(String contentFile) {
        ContentFile = contentFile;
    }
}
