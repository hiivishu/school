package app.com.shiksha.model;

/**
 * Created by vishal on 12/3/16.
 */

public class Result {

    private int marks_obtained;
    private String grade;
    private String subject_name;
    private String exam_name;
    private  int max_marks;
    private String totalgrade;

    public int getMarks_obtained() {
        return marks_obtained;
    }

    public void setMarks_obtained(int marks_obtained) {
        this.marks_obtained = marks_obtained;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getSubject_name() {
        return subject_name;
    }

    public void setSubject_name(String subject_name) {
        this.subject_name = subject_name;
    }

    public String getExam_name() {
        return exam_name;
    }

    public void setExam_name(String exam_name) {
        this.exam_name = exam_name;
    }

    public int getMax_marks() {
        return max_marks;
    }

    public void setMax_marks(int max_marks) {
        this.max_marks = max_marks;
    }

    public String getTotalgrade() {
        return totalgrade;
    }

    public void setTotalgrade(String totalgrade) {
        this.totalgrade = totalgrade;
    }
}
