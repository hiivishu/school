package app.com.shiksha.model;

/**
 * Created by vishal on 1/16/17.
 */

public class Chat_room_msg {

    private String msg;
    private String name;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
