package app.com.shiksha.model;

/**
 * Created by vishal on 12/8/16.
 */

public class HomeWork {

    public static final int DATE = 0;
    public static final int HOMEWORK_DETAILS = 1;

    private int type;
    private String date;
    private int id;
    private String title;
    private String description;
    private String submission_date;
    private int submission_status;
    private int section_id;
    private int subject_id;
    private String created_at;
    private String subject;
    private String homework_file_url;
    private String view_Details;
    private String homework_status;

//
//    public HomeWork (String date,String description,int type){
//        this.date = date;
//        this.description =description;
//        this.type = type;
//    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubmission_date() {
        return submission_date;
    }

    public void setSubmission_date(String submission_date) {
        this.submission_date = submission_date;
    }

    public int getSubmission_status() {
        return submission_status;
    }

    public void setSubmission_status(int submission_status) {
        this.submission_status = submission_status;
    }

    public int getSection_id() {
        return section_id;
    }

    public void setSection_id(int section_id) {
        this.section_id = section_id;
    }

    public int getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(int subject_id) {
        this.subject_id = subject_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getHomework_file_url() {
        return homework_file_url;
    }

    public void setHomework_file_url(String homework_file_url) {
        this.homework_file_url = homework_file_url;
    }


    public String getView_Details() {
        return view_Details;
    }

    public void setView_Details(String view_Details) {
        this.view_Details = view_Details;
    }

    public String getHomework_status() {
        return homework_status;
    }

    public void setHomework_status(String homework_status) {
        this.homework_status = homework_status;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
