package app.com.shiksha.model;

/**
 * Created by vishal on 11/18/16.
 */

public class Comment {

    private int id;
    private int post_id;
    private int guardian_id;
    private String comment;
    private String created_at;
    private String guardian_image_url;
    private String guardian_name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPost_id() {
        return post_id;
    }

    public void setPost_id(int post_id) {
        this.post_id = post_id;
    }

    public int getGuardian_id() {
        return guardian_id;
    }

    public void setGuardian_id(int guardian_id) {
        this.guardian_id = guardian_id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getGuardian_image_url() {
        return guardian_image_url;
    }

    public void setGuardian_image_url(String guardian_image_url) {
        this.guardian_image_url = guardian_image_url;
    }

    public String getGuardian_name() {
        return guardian_name;
    }

    public void setGuardian_name(String guardian_name) {
        this.guardian_name = guardian_name;
    }
}
