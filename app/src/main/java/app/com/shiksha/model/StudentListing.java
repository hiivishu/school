package app.com.shiksha.model;

/**
 * Created by vishal on 2/14/17.
 */

public class StudentListing {

    private  int id ;
    private String student_name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }
}
