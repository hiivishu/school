package app.com.shiksha.model;

/**
 * Created by vishal on 1/4/17.
 */

public class Chat {

    private int id;
    private String name_teacher;
    private String teacher_image_url;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName_teacher() {
        return name_teacher;
    }

    public void setName_teacher(String name_teacher) {
        this.name_teacher = name_teacher;
    }

    public String getTeacher_image_url() {
        return teacher_image_url;
    }

    public void setTeacher_image_url(String teacher_image_url) {
        this.teacher_image_url = teacher_image_url;
    }
}
