package app.com.shiksha.model;

/**
 * Created by vishal on 12/5/16.
 */

public class NewEvents {

    private int invitedid ;

    private String invitedevent_name;

    private  String invitedevent_place;

    private String inviteddescription;

    private String inviteddate;

    private String invitedtime;

    private String invitedcreated_at;

    private String invitedevent_image_url;

    private int invitedpeople_count;



    public String getInvitedevent_name() {
        return invitedevent_name;
    }

    public void setInvitedevent_name(String invitedevent_name) {
        this.invitedevent_name = invitedevent_name;
    }

    public String getInvitedevent_place() {
        return invitedevent_place;
    }

    public void setInvitedevent_place(String invitedevent_place) {
        this.invitedevent_place = invitedevent_place;
    }

    public String getInviteddescription() {
        return inviteddescription;
    }

    public void setInviteddescription(String inviteddescription) {
        this.inviteddescription = inviteddescription;
    }

    public String getInviteddate() {
        return inviteddate;
    }

    public void setInviteddate(String inviteddate) {
        this.inviteddate = inviteddate;
    }

    public String getInvitedtime() {
        return invitedtime;
    }

    public void setInvitedtime(String invitedtime) {
        this.invitedtime = invitedtime;
    }

    public String getInvitedcreated_at() {
        return invitedcreated_at;
    }

    public void setInvitedcreated_at(String invitedcreated_at) {
        this.invitedcreated_at = invitedcreated_at;
    }

    public String getInvitedevent_image_url() {
        return invitedevent_image_url;
    }

    public void setInvitedevent_image_url(String invitedevent_image_url) {
        this.invitedevent_image_url = invitedevent_image_url;
    }

    public int getInvitedid() {
        return invitedid;
    }

    public void setInvitedid(int invitedid) {
        this.invitedid = invitedid;
    }


    public int getInvitedpeople_count() {
        return invitedpeople_count;
    }

    public void setInvitedpeople_count(int invitedpeople_count) {
        this.invitedpeople_count = invitedpeople_count;
    }
}
