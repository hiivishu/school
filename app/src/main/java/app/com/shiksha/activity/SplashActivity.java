package app.com.shiksha.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import app.com.shiksha.MainActivity;
import app.com.shiksha.R;
import app.com.shiksha.helper.ConnectivityReceiver;

public class SplashActivity extends AppCompatActivity  implements ConnectivityReceiver.ConnectivityReceiverListener {

    public static final int SPLASH_TIME_OUT = 4 * 1000;
    public int otpCancel;
    Context context;
    SharedPreferences sharedPreferences = null;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sharedPreferences = getSharedPreferences("Login_pref", 0);
        editor = sharedPreferences.edit();
//        pref = getSharedPreferences("Login_pref",1);
//        editor = pref.edit();
//        sessionManager = new SessionManager(getApplicationContext());
        context = getApplicationContext();
//        otpCancel = sharedPreferences.getInt("otpCancel", 0);

        // Manually checking internet connection
        checkConnection();
    }
    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        if (isConnected){
            splashAndGoNext();
        }else {
            Intent intent = new Intent(getApplicationContext(),NoInternetconnectionActivity.class);
            startActivity(intent);
            finish();
        }
    }

    public void splashAndGoNext() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {


                SharedPreferences pref = getSharedPreferences("ActivityPREF", Context.MODE_PRIVATE);

//                if (otpCancel == 0){
//                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
//                    startActivity(intent);
//                    overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
//                    finish();
//
//                }else
                if (pref.getBoolean("activity_executed", false)) {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();

                } else {
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                    finish();

                }


//                Log.e("sessinon",""+sessionManager.isLoggedIn());
//
//                if (sessionManager.isLoggedIn()){
//                    Intent intent = new Intent(SplashActivity.this,MainActivity.class);
//                    startActivity(intent);
//                    finish();
//                }else {
//                    Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
//                    startActivity(intent);
//                    finish();
//                }

//                    MyUtils.logThis("SplashScreen - App is initialised");
//                    Intent i = new Intent(context, MainActivity.class);
//                    startActivity(i);
//                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    // Method to manually check connection status

}
