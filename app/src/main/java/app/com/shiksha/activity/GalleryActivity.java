package app.com.shiksha.activity;

import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import android.view.View.OnClickListener;
import app.com.shiksha.R;
import app.com.shiksha.fragment.GalleryImagesFragment;
import app.com.shiksha.fragment.GalleryVideoFragment;

public class GalleryActivity extends AppCompatActivity {
    private Toolbar toolbar;
    Button btn1;
    Button btn2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        toolbar = (Toolbar)findViewById(R.id.toolbar_gallery);
        toolbar.setNavigationIcon(R.drawable.navigation_icon);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        OnClickListener listener = new OnClickListener() {
            public void onClick(View view) {
                Fragment fragment = null;
                if(view == findViewById(R.id.images_button)){
                    fragment = new GalleryImagesFragment();
                    btn1.setBackgroundResource(R.drawable.button_with_shadow1);
                    btn2.setBackgroundResource(R.drawable.button_with_shadow);


                } else {
                    fragment = new GalleryVideoFragment();
                    btn2.setBackgroundResource(R.drawable.button_with_shadow1);
                    btn1.setBackgroundResource(R.drawable.button_with_shadow);

                }
                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.output, fragment);
                transaction.commit();
            }
        };
        btn1 = (Button)findViewById(R.id.images_button);
        btn1.setOnClickListener(listener);
        Typeface font6 = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        btn1.setTypeface(font6);
        btn2 = (Button)findViewById(R.id.video_button);
        btn2.setOnClickListener(listener);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        btn2.setTypeface(font);
    }
//        Button button = (Button)findViewById(R.id.images_button);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Fragment fragment =  new GalleryImagesFragment();;
//                FragmentManager manager = getSupportFragmentManager();
//                FragmentTransaction transaction = manager.beginTransaction();
//                transaction.replace(R.id.output, fragment);
//                transaction.commit();
//            }
//        });
//
//        Button button1 = (Button)findViewById(R.id.video_button);
//        button1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Fragment fragment = new GalleryVideoFragment();
//                FragmentManager manager = getSupportFragmentManager();
//                FragmentTransaction transaction = manager.beginTransaction();
//                transaction.replace(R.id.output, fragment);
//                transaction.commit();
//            }
//        });
    }



