package app.com.shiksha.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.com.shiksha.R;
import app.com.shiksha.adapter.CommentAdapter;
import app.com.shiksha.helper.MyUtils;
import app.com.shiksha.helper.UrlSupport;
import app.com.shiksha.model.Comment;

public class CommentActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    int post_id;
    SharedPreferences sharedPreferences = null;
    SharedPreferences.Editor editor;
    int user_id;
    String gurdian_id;
    String post_id_value;
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private CommentAdapter commentAdapter;
    private List<Comment> comment_data;
    private LinearLayout linlaheaderprogress, no_internet_lay;
    private EditText send_comment_data;
    private TextView submit_data;
    private String comment_final_data;
    private ProgressDialog progressDialog;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        toolbar = (Toolbar) findViewById(R.id.toolbar_comment);
//        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.navigation_icon);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            post_id = extras.getInt("post_id");

        }
        post_id_value = String.valueOf(post_id);
        sharedPreferences = getSharedPreferences("Login_pref", 0);
        editor = sharedPreferences.edit();
        user_id = sharedPreferences.getInt("id", 0);

        gurdian_id = String.valueOf(user_id);


        Log.e("comment", gurdian_id + post_id_value.toString());

        if (MyUtils.hasActiveInternetConnection(getApplicationContext())) {
            recyclerView = (RecyclerView) findViewById(R.id.comment_recycleview);
            recyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(layoutManager);
            swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout_comment);

            swipeRefreshLayout.setOnRefreshListener(this);

            /**
             * Showing Swipe Refresh animation on activity create
             * As animation won't start on onCreate, post runnable is used
             */
            swipeRefreshLayout.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            swipeRefreshLayout.setRefreshing(true);

                                            CommentApi();
                                        }
                                    }
            );


        /*Request Comment API*/

            CommentApi();
        } else {
            no_internet_lay = (LinearLayout) findViewById(R.id.no_Internet_lay_comment);
            no_internet_lay.setVisibility(View.VISIBLE);
        }
        send_comment_data = (EditText) findViewById(R.id.comment_data_box);
        submit_data = (TextView) findViewById(R.id.submit_comment);

        submit_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comment_final_data = send_comment_data.getText().toString().trim();


                if (!comment_final_data.isEmpty()) {


                    InputMethodManager inputManager = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);

                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                    PostCommentApi();
                } else {
                    Toast.makeText(getApplicationContext(), "Add Comment", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private void PostCommentApi() {
        //Progress Dialog.
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Posting...");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlSupport.Comment_posting,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("LoginResponse", "" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String sucess = jsonObject.getString("success");
                            if (sucess.equals("true")) {
                                progressDialog.dismiss();
                                CommentApi();
                                send_comment_data.setText("");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(CommentActivity.this, "Something Went Wrong", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("comment", comment_final_data);
                params.put("guardian_id", gurdian_id);
                params.put("post_id", post_id_value);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    private void CommentApi() {


        StringRequest stringRequest = new StringRequest(Request.Method.GET, UrlSupport.Comment_Listing + post_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("commentapi", UrlSupport.Comment_Listing + post_id);
                        Log.e("LoginResponse", "" + response);
                        comment_data = new ArrayList<>();
                        try {
                            JSONObject jsonObject1 = new JSONObject(response);
                            String success = jsonObject1.getString("success");
                            if (success.equals("true")) {
                                JSONArray jsonarray1 = jsonObject1.getJSONArray("result");
                                for (int i = 0; i < jsonarray1.length(); i++) {
                                    Comment comment = new Comment();
                                    JSONObject jsonobject2 = jsonarray1.getJSONObject(i);
                                    int id = jsonobject2.getInt("id");
                                    int post_id = jsonobject2.getInt("post_id");
                                    int guardian_id = jsonobject2.getInt("guardian_id");
                                    String comment1 = jsonobject2.getString("comment");
                                    String created_at = jsonobject2.getString("created_at");
                                    String guardian_name = jsonobject2.getString("guardian_name");
                                    String guardian_image_url = jsonobject2.getString("guardian_image_url");
                                    comment.setGuardian_image_url(guardian_image_url);
                                    comment.setGuardian_name(guardian_name);
                                    comment.setComment(comment1);
                                    comment.setCreated_at(created_at);
                                    comment.setId(id);
                                    comment.setGuardian_id(guardian_id);
                                    comment.setPost_id(post_id);
                                    comment_data.add(comment);

                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        commentAdapter = new CommentAdapter(getApplicationContext(), comment_data);
                        recyclerView.setAdapter(commentAdapter);
                        swipeRefreshLayout.setRefreshing(false);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Something Went Wrong", Toast.LENGTH_LONG).show();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }) {
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    @Override
    public void onRefresh() {
        CommentApi();
    }
}
