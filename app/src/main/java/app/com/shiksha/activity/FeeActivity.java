package app.com.shiksha.activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.adapter.FeeDataAdapter;
import app.com.shiksha.helper.ConnectivityReceiver;
import app.com.shiksha.helper.UrlSupport;
import app.com.shiksha.model.FeeData;

public class FeeActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    String student_id;
    boolean isConnected;
    private Toolbar toolbar;
    private TextView textView, textView1, textView2;
    private RecyclerView recyclerView;
    private FeeDataAdapter feedataAdapter;
    private List<FeeData> fee_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fee);
        toolbar = (Toolbar) findViewById(R.id.toolbar_feedetail);
        toolbar.setNavigationIcon(R.drawable.navigation_icon);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        isConnected = ConnectivityReceiver.isConnected();
        student_id = getIntent().getStringExtra("student_id");

        textView = (TextView) findViewById(R.id.balance_amount);
        Typeface font6 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textView.setTypeface(font6);
        textView1 = (TextView) findViewById(R.id.nill);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textView1.setTypeface(font);
        textView2 = (TextView) findViewById(R.id.fee_transc);
        Typeface font1 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textView2.setTypeface(font1);


        if (isConnected) {
            recyclerView = (RecyclerView) findViewById(R.id.fee_recycle_lay);
            recyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(layoutManager);

            FeedDetailsApi();


        }


    }

    private void FeedDetailsApi() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, UrlSupport.Feedetails+"2",
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("AttendanceResponse", "" + response);

                        fee_data = new ArrayList<>();

                        try {
                            JSONObject jsonobject = new JSONObject(response);
                            String sucess = jsonobject.getString("success");
                            if (sucess.equals("true")) {
                                int balance = jsonobject.getInt("balance");
                                JSONObject jsonobject1 = jsonobject.getJSONObject("result");
                                JSONArray jsonArray = jsonobject1.getJSONArray("feedetails");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    FeeData feedata = new FeeData();
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    String remark = jsonObject.getString("remarks");
                                    String submission_date = jsonObject.getString("submission_date");

                                    feedata.setRemarks(remark);
                                    feedata.setSubmission_date(submission_date);
                                    fee_data.add(feedata);
                                    Log.e("remark", remark);
                                }
                                setfeedetailsdata(balance);
                            } else if (sucess.equals("false")) {
                                String error = jsonobject.getString("error");
                                Toast.makeText(getApplicationContext(), error, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        feedataAdapter = new FeeDataAdapter(getApplicationContext(), fee_data);
                        recyclerView.setAdapter(feedataAdapter);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getApplicationContext(),"Something Went Wrong",Toast.LENGTH_LONG).show();
                    }
                }) {
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    private void setfeedetailsdata(int balance) {
        TextView textview = (TextView) findViewById(R.id.nill);
        textview.setText(String.valueOf(balance));
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}
