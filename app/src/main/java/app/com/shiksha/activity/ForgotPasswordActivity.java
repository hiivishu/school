package app.com.shiksha.activity;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import app.com.shiksha.R;
import app.com.shiksha.helper.UrlSupport;

public class ForgotPasswordActivity extends AppCompatActivity {

    private EditText edittext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        TextView textView = (TextView)findViewById(R.id.cancel_forgot);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        TextView textView1 = (TextView)findViewById(R.id.submit_forgot);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        textView.setTypeface(font);
        Typeface font2 = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        textView1.setTypeface(font2);
        TextView textview3 = (TextView)findViewById(R.id.forgot_text);
        Typeface font3 = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        textview3.setTypeface(font3);
        TextView textView4 = (TextView)findViewById(R.id.enter_text);
        Typeface font4 = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        textView4.setTypeface(font4);
        edittext = (EditText)findViewById(R.id.email_forgot) ;
        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ForgotPasswordApi();
            }
        });
    }

    private void ForgotPasswordApi() {
        final ProgressDialog loading = ProgressDialog.show(this,"Sending...","Please wait...",false,false);
        loading.setCanceledOnTouchOutside(false);
        String edittextdata = edittext.getText().toString();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, UrlSupport.ForgotPasswordApi+edittextdata,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("Forgotpassword", "" + response);
                        try {
                            JSONObject json = new JSONObject(response);
                            String success = json.getString("success");
                            if (success.equals("true")){
                                JSONObject js = json.getJSONObject("result");
                                String message = js.getString("message");

                                Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        loading.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loading.dismiss();
                        Toast.makeText(getApplicationContext(),"Something Went Wrong",Toast.LENGTH_LONG).show();
                    }
                }) {
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);

    }
}
