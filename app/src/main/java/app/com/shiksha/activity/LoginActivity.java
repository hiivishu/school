package app.com.shiksha.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;

import app.com.shiksha.MainActivity;
import app.com.shiksha.R;
import app.com.shiksha.helper.ConnectivityReceiver;
import app.com.shiksha.helper.SessionManager;
import app.com.shiksha.helper.UrlSupport;

public class LoginActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener{

    private static final String TAG = LoginActivity.class.getSimpleName();
    public SessionManager sessionManager;
    SharedPreferences sharedPreferences = null;
    SharedPreferences.Editor editor;
    int stu_id;
    String student_name;
    boolean isConnected;
    private RelativeLayout btnLogin;
    private EditText inputEmail;
    private EditText inputPassword;
    private ProgressDialog progressDialog;
    private String email, password, macAddr;
    private String fcmid;
    private TextView emailtext,passwordtext;
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    private boolean sentToSettings = false;
    private SharedPreferences permissionStatus;
    String prime_phone,sec_phone,phone;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        isConnected = ConnectivityReceiver.isConnected();
//        fcmid = FirebaseInstanceId.getInstance().getToken();
        sharedPreferences = getSharedPreferences("Login_pref", 0);
        editor = sharedPreferences.edit();
        inputEmail = (EditText) findViewById(R.id.input_email);
        inputPassword = (EditText) findViewById(R.id.input_password);
        btnLogin = (RelativeLayout) findViewById(R.id.sigin_lay);
        permissionStatus = getSharedPreferences("permissionStatus",MODE_PRIVATE);

        inputEmail.setText("Stu4228");
        inputPassword.setText("Stu005680");
//

//        sessionManager = new SessionManager(getApplicationContext());

        if (ActivityCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setTitle("Need Storage Permission");
                builder.setMessage("This app needs storage permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else if (permissionStatus.getBoolean(Manifest.permission.WRITE_EXTERNAL_STORAGE,false)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setTitle("Need Storage Permission");
                builder.setMessage("This app needs storage permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getBaseContext(), "Go to Permissions to Grant Storage", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
            }

            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.WRITE_EXTERNAL_STORAGE,true);
            editor.commit();


        } else {
            //You already have the permission, just go ahead.

        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                email = inputEmail.getText().toString().trim();
                password = inputPassword.getText().toString().trim();

//                Intent intent= new Intent(getApplicationContext(),MainActivity.class);
//                startActivity(intent);

             if (!email.isEmpty() && !password.isEmpty() && isConnected) {

                    //Perform Login.
                    Login();


                } else if (email.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Email error", Toast.LENGTH_LONG).show();

                } else if (password.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Password Error", Toast.LENGTH_LONG).show();
                } else if (!isConnected) {
                    Toast.makeText(getApplicationContext(), "Internet Error", Toast.LENGTH_LONG).show();
                }
            }
        });

        TextView textView = (TextView)findViewById(R.id.forgot_password);
        Typeface font6 = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        textView.setTypeface(font6);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });
        TextView textView1 = (TextView)findViewById(R.id.btnLogin);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        textView1.setTypeface(font);
        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN ) {

        }
        else {
            textView1.setLetterSpacing((float) 0.1);
        }
//
        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                email = inputEmail.getText().toString().trim();
                password = inputPassword.getText().toString().trim();



                if (!email.isEmpty() && !password.isEmpty() && isConnected) {

                    //Perform Login.
                    Login();


                } else if (email.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Email error", Toast.LENGTH_LONG).show();

                } else if (password.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Password Error", Toast.LENGTH_LONG).show();
                } else if (!isConnected) {
                    Toast.makeText(getApplicationContext(), "Internet Error", Toast.LENGTH_LONG).show();
                }
            }
        });

        emailtext = (TextView)findViewById(R.id.email_text_login);
        Typeface font1 = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        emailtext.setTypeface(font1);
//        emailtext.setLetterSpacing((float) 0.3);
        passwordtext = (TextView)findViewById(R.id.passwordtextlogin);
        Typeface font2 = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        passwordtext.setTypeface(font2);
//        passwordtext.setLetterSpacing((float) 0.3);
    }


    private void Login() {

        //Progress Dialog.
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Working.....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        Log.e("UrlToHit",UrlSupport.Login+"UserId="+email+"+&Password="+password+"");

        final StringRequest stringRequest = new StringRequest(Request.Method.GET, UrlSupport.Login+"UserId="+email+"+&Password="+password+"",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("LoginResponse", "" + response);


                        try {
                            JSONObject jsonobj = new JSONObject(response);
                            JSONObject jsonObject = jsonobj.getJSONObject("Login");
                            JSONObject jsonObject1 = jsonObject.getJSONObject("Status");
                            String sucess = jsonObject1.getString("Type");
                            if (sucess.equals("success")) {

                                JSONObject jsonObject2 = jsonObject.getJSONObject("Data");

                                    String name = jsonObject2.getString("StudentName");
                                    String ProfileImage = jsonObject2.getString("ImagePath");
                                    String profileimg = ProfileImage.replaceAll(" ","");
                                    editor.putString("name",name);
                                    editor.putString("profileimage_url",profileimg);
                                    editor.apply();


                                editor.putString("email", email);
                                editor.putString("password", password);
                                editor.apply();
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                //set activity_executed inside insert() method.
                                SharedPreferences pref = getSharedPreferences("ActivityPREF", Context.MODE_PRIVATE);
                                SharedPreferences.Editor edt = pref.edit();
                                edt.putBoolean("activity_executed", true);
                                edt.commit();
//                                Log.e("sessioninloginmain",""+sessionManager.isLoggedIn());
                                intent.putExtra("emailData", email);
                                intent.putExtra("passwordData", password);
                                startActivity(intent);
                                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                                finish();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
//                        Log.e("studentnew1",""+student_name+stu_id);
                        progressDialog.dismiss();
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        progressDialog.dismiss();
                    }
                }) {

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void SendOtp() {


        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlSupport.SendOtp,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("LoginResponse", "" + response);

                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Something Went Wrong Please Try Again", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}
