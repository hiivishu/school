package app.com.shiksha.activity;

import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.adapter.SyllabusAdapter;
import app.com.shiksha.helper.ConnectivityReceiver;
import app.com.shiksha.helper.UrlSupport;
import app.com.shiksha.model.Syllabus;

public class SyllabusActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    private Toolbar toolbar;
    boolean isConnected;
    String student_id,ClassId,ClassName,Section;
    String syllabus_file_url;
    RecyclerView syllabusRecyclerview;
    SyllabusAdapter syllabusAdapter;
    List<Syllabus> syllabusList_data;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_syllabus);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Working.....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        toolbar = (Toolbar)findViewById(R.id.toolbar_syllabus);
        toolbar.setNavigationIcon(R.drawable.navigation_icon);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        isConnected = ConnectivityReceiver.isConnected();
        student_id = getIntent().getStringExtra("student_id");
        ClassId = getIntent().getStringExtra("ClassId");
        ClassName = getIntent().getStringExtra("ClassName");
        Section = getIntent().getStringExtra("Section");


        if (isConnected){
            loadSyllabusApi();
            syllabusRecyclerview = (RecyclerView)findViewById(R.id.syllabusRecyclerview);
            syllabusRecyclerview.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            syllabusRecyclerview.setLayoutManager(layoutManager);

        }

    }

    private void loadSyllabusApi() {
        syllabusList_data = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, UrlSupport.SyllabusApi+"ClassId="+ClassId,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("SyllabusResponse", "" + response);
                        try {
                            JSONObject jsonobject = new JSONObject(response);
                            JSONObject jsonObject = jsonobject.getJSONObject("Syllabus");



                                JSONArray jsonArray = jsonObject.getJSONArray("Data");
                                if (jsonArray.length()==0){
                                    Toast.makeText(getApplicationContext(),"No Syllabus Found",Toast.LENGTH_LONG).show();
                                }else {
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        Syllabus syllabus = new Syllabus();
                                        JSONObject jsonobject1 = jsonArray.getJSONObject(i);

                                        String  syllabus_file_url = jsonobject1.getString("FilePath");
                                        String syllabusFile = syllabus_file_url.replaceAll(" ","");
                                        syllabus.setContentFile(syllabusFile);
                                        syllabusList_data.add(syllabus);
                                        Log.e("jsio",jsonobject1.toString());


                                    }

                                }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        syllabusAdapter = new SyllabusAdapter(getApplicationContext(),syllabusList_data);
                        syllabusRecyclerview.setAdapter(syllabusAdapter);
                        progressDialog.dismiss();


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();

                    }
                }) {
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    private void setSyllabusdata(final String syllabus_file_url, final String syllabus_filename, int class_id, final String class_name) {
        TextView textview = (TextView)findViewById(R.id.class_name);
        textview.setText(String.valueOf(class_name));
        Typeface font6 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textview.setTypeface(font6);
        TextView textview1 = (TextView)findViewById(R.id.don);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textview1.setTypeface(font);
        TextView textview2 = (TextView)findViewById(R.id.pdf);
        Typeface font3 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textview2.setTypeface(font3);

        RelativeLayout relativelay = (RelativeLayout)findViewById(R.id.downloadpdf);
        relativelay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isConnected) {

                    //download file
                    Toast.makeText(getApplicationContext(), "Downloading started", Toast.LENGTH_SHORT).show();
                    DownloadManager.Request request = new DownloadManager.Request(Uri.parse(syllabus_file_url));
                    request.setDescription("Downloading Syllabus");
                    request.setTitle(class_name+"  "+"Syllabus");
                    // in order for this if to run, you must use the android 3.2 to compile your app
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        request.allowScanningByMediaScanner();
                        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                    }
                    request.setDestinationInExternalPublicDir("Schulerz/", class_name + " - " + syllabus_filename );

                    // get download service and enqueue file
                    DownloadManager manager = (DownloadManager) getApplicationContext().getSystemService(Context.DOWNLOAD_SERVICE);
                    manager.enqueue(request);
                    Toast.makeText(getApplicationContext(), "Downloading started", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.no_internet_warning, Toast.LENGTH_LONG).show();
                }
            }
        });

    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}
