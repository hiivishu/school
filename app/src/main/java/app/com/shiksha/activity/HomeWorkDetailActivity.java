package app.com.shiksha.activity;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import app.com.shiksha.R;

public class HomeWorkDetailActivity extends AppCompatActivity {
String description,subject;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_work_detail);
        description = getIntent().getStringExtra("description");
        subject = getIntent().getStringExtra("subject");
        TextView textView = (TextView)findViewById(R.id.t);
        textView.setText(description);
        TextView textView2 = (TextView)findViewById(R.id.details_home_subject);
        textView2.setText(subject);

        TextView textView1 = (TextView)findViewById(R.id.close_frag21);
        textView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
         TextView textView3 = (TextView)findViewById(R.id.pro21);
        Typeface font4 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textView3.setTypeface(font4);
        Typeface font5 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textView.setTypeface(font5);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textView1.setTypeface(font);
        Typeface font2 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textView2.setTypeface(font2);

    }
}
