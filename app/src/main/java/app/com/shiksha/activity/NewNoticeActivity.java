package app.com.shiksha.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.adapter.NewNoticesAdapter;
import app.com.shiksha.helper.ConnectivityReceiver;
import app.com.shiksha.helper.UrlSupport;
import app.com.shiksha.model.NewNotices;

public class NewNoticeActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    private Toolbar toolbar;
    boolean isConnected;
    private RecyclerView recyclerView;
    private List<NewNotices> notices_data;
    private NewNoticesAdapter newnoticeadapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_notice);
        toolbar = (Toolbar) findViewById(R.id.toolbar_new_notices);
        toolbar.setNavigationIcon(R.drawable.navigation_icon);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        isConnected = ConnectivityReceiver.isConnected();
        if (isConnected) {
            recyclerView = (RecyclerView) findViewById(R.id.newnotice_recycle_lay);
            recyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(layoutManager);
            NewNoticeApi();
        }
//        TextView textView = (TextView)findViewById(R.id.invited_heading);
//        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
//        textView.setTypeface(font);
//        TextView textView1 = (TextView)findViewById(R.id.date_notice);
//        Typeface font1 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
//        textView1.setTypeface(font1);
//        TextView textView2 =(TextView)findViewById(R.id.invited_heading12);
//        Typeface font2 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
//        textView2.setTypeface(font2);
//        TextView textView3 = (TextView)findViewById(R.id.date12);
//        Typeface font3 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
//        textView3.setTypeface(font3);
//        TextView textView4 = (TextView)findViewById(R.id.venue12);
//        Typeface font4 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
//        textView4.setTypeface(font4);
    }

    private void NewNoticeApi() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, UrlSupport.NewNoticesApi,
                new Response.Listener<String>() {


                    @Override
                    public void onResponse(String response) {
                        Log.e("NewNoticeResponse", "" + response);
                        notices_data = new ArrayList<>();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            if (success.equals("true")){
                                JSONArray jsonarray = jsonObject.getJSONArray("result");
                                for (int i = 0; i < jsonarray.length(); i++) {
                                    NewNotices newNotices = new NewNotices();

                                    JSONObject jsonobject = jsonarray.getJSONObject(i);
                                    String subject = jsonobject.getString("subject");
                                    String description = jsonobject.getString("description");
                                    newNotices.setSubject(subject);
                                    newNotices.setDescription(description);
                                    notices_data.add(newNotices);

                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        newnoticeadapter = new NewNoticesAdapter(getApplicationContext(),notices_data);
                        recyclerView.setAdapter(newnoticeadapter);
                        newnoticeadapter.notifyDataSetChanged();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getApplicationContext(),"Something Went Wrong",Toast.LENGTH_LONG).show();
                    }
                }) {
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}
