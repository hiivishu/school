package app.com.shiksha.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.adapter.GurdianChildViewAdapter;
import app.com.shiksha.helper.ConnectivityReceiver;
import app.com.shiksha.helper.UrlSupport;
import app.com.shiksha.model.GurdainChildView;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener{
    private Toolbar toolbar;
    SharedPreferences sharedPreferences ;
    SharedPreferences.Editor editor;
    String profilepic,username,email,prime_phone,address,type;
    private RecyclerView recyclerView;
    private GurdianChildViewAdapter gurdianChildViewAdapter;
    boolean isConnected;
    private int id;
    private List<GurdainChildView> gurdianchild_data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        toolbar = (Toolbar)findViewById(R.id.toolbar_profile);
        toolbar.setNavigationIcon(R.drawable.navigation_icon);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        sharedPreferences = getSharedPreferences("Login_pref", 0);
        editor = sharedPreferences.edit();
        profilepic = sharedPreferences.getString("profileimage_url", "");
        username = sharedPreferences.getString("name","");
        email = sharedPreferences.getString("email","");
        prime_phone = sharedPreferences.getString("prime_phone","");
        address = sharedPreferences.getString("address","");
        id = sharedPreferences.getInt("id",0);
//        type = sharedPreferences.getString("type","");
        type = "Guardian";
        isConnected = ConnectivityReceiver.isConnected();
        if (isConnected){

            recyclerView = (RecyclerView)findViewById(R.id.gurdian_child_recyclerview);
            recyclerView.setHasFixedSize(true);
            LinearLayoutManager horizontalLayoutManagaer
                    = new LinearLayoutManager(ProfileActivity.this, LinearLayoutManager.HORIZONTAL, false);
            recyclerView.setLayoutManager(horizontalLayoutManagaer);

            if (type.equals("Guardian")){
                GurdianChildApi();
            }else {
                // Do nothing
            }

        }



        CircleImageView circleImageView = (CircleImageView)findViewById(R.id.pro_pic_profile);
//        CircleImageView circleImageView1 = (CircleImageView)findViewById(R.id.child_pic1);
//        CircleImageView circleImageView2 = (CircleImageView)findViewById(R.id.child_pic2);
//        Picasso.with(getApplicationContext())
//                .load(R.drawable.chil2)
//                .placeholder(R.drawable.default_avatar) //this is optional the image to display while the url image is downloading
//                .into(circleImageView2);
//        Picasso.with(getApplicationContext())
//                .load(R.drawable.child1)
//                .placeholder(R.drawable.default_avatar) //this is optional the image to display while the url image is downloading
//                .into(circleImageView1);

        Picasso.with(getApplicationContext())
                .load(R.drawable.default_avatar)
                .placeholder(R.drawable.default_avatar) //this is optional the image to display while the url image is downloading
                .into(circleImageView);

//        TextView textView9 = (TextView)findViewById(R.id.child_name1);
//        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
//        textView9.setTypeface(font);
//        TextView textView8 = (TextView)findViewById(R.id.child_name2);
//        Typeface font4 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
//        textView8.setTypeface(font4);

        TextView textView = (TextView)findViewById(R.id.person_profile_name);
        Typeface font6 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textView.setTypeface(font6);
        textView.setText(username);
        TextView textView1 = (TextView)findViewById(R.id.input_email_profile);
        textView1.setText(email);
        TextView textView2 =(TextView)findViewById(R.id.input_phone_no);
        textView2.setText(prime_phone);
        ImageView imageView = (ImageView)findViewById(R.id.edit_profile);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),EditProfileActivity.class);
//                intent.putExtra("email",email);
//                intent.putExtra("name",username);
//                intent.putExtra("pro_pic",profilepic);
//                intent.putExtra("phone_no",prime_phone);
                startActivity(intent);
            }
        });
        TextView textView3 = (TextView)findViewById(R.id.emailtext_profile);
        Typeface font7 = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        textView3.setTypeface(font7);
        TextView textView4 = (TextView)findViewById(R.id.phoneno_text);
        Typeface font1 = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        textView4.setTypeface(font1);
    }

    private void GurdianChildApi() {

        Log.e("urlcheck",UrlSupport.Dashboard_data+"2");
        StringRequest stringRequest = new StringRequest(Request.Method.GET, UrlSupport.Dashboard_data+"30",
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("GurdianchildResponse", "" + response);

                        gurdianchild_data = new ArrayList<>();
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String success = jsonObject.getString("success");
                            if (success.equals("true")){
                                JSONObject jsonobj = jsonObject.getJSONObject("result");
                                JSONArray jsonarray = jsonobj.getJSONArray("student_data");

                                for (int i = 0; i<jsonarray.length();i++){
                                    GurdainChildView gurdian = new GurdainChildView();
                                    JSONObject json = jsonarray.getJSONObject(i);

                                    int student_id = json.getInt("id");
                                    String name = json.getString("name");
                                    gurdian.setStudent_id(student_id);
                                    gurdian.setName(name);
                                    gurdianchild_data.add(gurdian);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        gurdianChildViewAdapter = new GurdianChildViewAdapter(getApplicationContext(),gurdianchild_data);
                        recyclerView.setAdapter(gurdianChildViewAdapter);


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getApplicationContext(),"Something Went Wrong",Toast.LENGTH_LONG).show();
                    }
                }) {
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}
