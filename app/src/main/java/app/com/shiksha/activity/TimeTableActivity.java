package app.com.shiksha.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.adapter.NewTimeTableAdapter;
import app.com.shiksha.helper.AnimatedExpandableListView;
import app.com.shiksha.helper.ConnectivityReceiver;
import app.com.shiksha.helper.UrlSupport;
import app.com.shiksha.model.LatestTimeTable;
import app.com.shiksha.model.NewTImeTable;

public class TimeTableActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    String student_id,ClassName,ClassId,Section;
    String subject_name;
    boolean isConnected;
    private Toolbar toolbar;
    private AnimatedExpandableListView listView;
    private ExampleAdapter adapter;
    RecyclerView newTimeTableRecyclerView;
    NewTimeTableAdapter newTimeTableAdapter;
    List<LatestTimeTable> newTImeTableList_data;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_table);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Working.....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        toolbar = (Toolbar) findViewById(R.id.toolbar_time_table);
//        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.navigation_icon);
        isConnected = ConnectivityReceiver.isConnected();
//        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        student_id = getIntent().getStringExtra("student_id");
        ClassName = getIntent().getStringExtra("ClassName");
        ClassId = getIntent().getStringExtra("ClassId");
        Section = getIntent().getStringExtra("Section");

        if (isConnected) {
            Timetableapi();
            newTimeTableRecyclerView = (RecyclerView)findViewById(R.id.newTimeTableRecyclerView);
            newTimeTableRecyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            newTimeTableRecyclerView.setLayoutManager(layoutManager);

        }


    }

    private void Timetableapi() {
        newTImeTableList_data = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, UrlSupport.TimeTableApi+"ClassId="+ClassId+"+&Section="+Section+"",
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("TimeTableResponse", "" + response);

                        try {
//                            JSONObject jsonobject = new JSONObject(response);
//                            String sucess = jsonobject.getString("success");
//                            if (sucess.equals("true")) {
//                                List<GroupItem> items = new ArrayList<GroupItem>();
//
//                                JSONArray jsonarray = jsonobject.getJSONArray("timetable_all");
//                                for (int i = 0; i < jsonarray.length(); i++) {
//
//                                    JSONObject jsonobject1 = jsonarray.getJSONObject(i);
//                                    String days = jsonobject1.getString("days");
//                                    Log.e("timedays", days);
//                                    GroupItem item = new GroupItem();
//
//                                    item.title = days;
//
//                                    JSONArray jsonarray1 = jsonobject1.getJSONArray("timetable");
//                                    for (int j = 0; j < jsonarray1.length(); j++) {
//
//                                        JSONObject jsonobject2 = jsonarray1.getJSONObject(j);
//                                        String from_time = jsonobject2.getString("from_time");
//                                        String to_time = jsonobject2.getString("to_time");
//                                        subject_name = jsonobject2.getString("subject_name");
//                                        Log.e("timedetail", from_time + to_time + subject_name);
//                                        ChildItem child = new ChildItem();
//                                        child.title = subject_name;
//                                        child.hint = from_time;
//                                        child.hint1 = to_time;
//
//                                        item.items.add(child);
//                                    }
//                                    items.add(item);
//                                }
//                                adapter = new ExampleAdapter(getApplicationContext());
//                                adapter.setData(items);
//
//                                listView = (AnimatedExpandableListView) findViewById(R.id.listViewtimetable);
//                                listView.setAdapter(adapter);


                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject jsonObject1 = jsonObject.getJSONObject("TimeTable");
//                            String Status = jsonObject.getString("Status");
                            JSONArray jsonArray = jsonObject1.getJSONArray("Data");
                            for (int i =0;i<jsonArray.length();i++){
                                LatestTimeTable newTImeTable = new LatestTimeTable();
                                JSONObject jsonObject3 = jsonArray.getJSONObject(i);
                                String FilePath = jsonObject3.getString("FilePath");
//                                String FromTime = jsonObject1.getString("FromTime");
//                                String ToTime = jsonObject1.getString("ToTime");
//                                String Day = jsonObject1.getString("Day");
//                                Log.e("day",Day);
//                                newTImeTable.setDay(Day);
//                                newTImeTable.setFromTime(FromTime);
//                                newTImeTable.setSubject(Subject);
//                                newTImeTable.setToTime(ToTime);
                                newTImeTable.setFilePath(FilePath);
                                newTImeTableList_data.add(newTImeTable);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        newTimeTableAdapter = new NewTimeTableAdapter(getApplicationContext(),newTImeTableList_data,ClassName);
                        newTimeTableRecyclerView.setAdapter(newTimeTableAdapter);
                        progressDialog.dismiss();
//                        listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
//
//                            @Override
//                            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
//                                // We call collapseGroupWithAnimation(int) and
//                                // expandGroupWithAnimation(int) to animate group
//                                // expansion/collapse.
//                                if (listView.isGroupExpanded(groupPosition)) {
//                                    listView.collapseGroupWithAnimation(groupPosition);
//                                } else {
//                                    listView.expandGroupWithAnimation(groupPosition);
//                                }
//                                return true;
//                            }
//
//                        });

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                     progressDialog.dismiss();
                    }
                }) {
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    private static class GroupItem {
        String title;
        List<ChildItem> items = new ArrayList<ChildItem>();
    }

    private static class ChildItem {
        String title;
        String hint;
        String hint1;
    }

    private static class ChildHolder {
        TextView title;
        TextView hint;
        TextView hint1;
    }

    private static class GroupHolder {
        TextView title;
    }

    /**
     * Adapter for our list of {@link GroupItem}s.
     */
    private class ExampleAdapter extends AnimatedExpandableListView.AnimatedExpandableListAdapter {
        private LayoutInflater inflater;

        private List<GroupItem> items;

        public ExampleAdapter(Context context) {
            inflater = LayoutInflater.from(context);
        }

        public void setData(List<GroupItem> items) {
            this.items = items;
        }

        @Override
        public ChildItem getChild(int groupPosition, int childPosition) {
            return items.get(groupPosition).items.get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getRealChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            ChildHolder holder;
            ChildItem item = getChild(groupPosition, childPosition);
            if (convertView == null) {
                holder = new ChildHolder();
                convertView = inflater.inflate(R.layout.list_item_time_table, parent, false);
                holder.title = (TextView) convertView.findViewById(R.id.textTitle);
                holder.hint = (TextView) convertView.findViewById(R.id.textHint);
                holder.hint1 = (TextView) convertView.findViewById(R.id.textHint1);
                Typeface font6 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
                holder.title.setTypeface(font6);
                Typeface font3 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
                holder.hint.setTypeface(font3);
                Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
                holder.hint1.setTypeface(font);
                convertView.setTag(holder);

            } else {
                holder = (ChildHolder) convertView.getTag();
            }

            if (item.title.equals("Physics")) {
                holder.title.setTextColor(ColorStateList.valueOf(Color.BLUE));
                holder.title.setText(item.title);
            } else if (item.title.equals("Chemistry")) {
                holder.title.setTextColor(ColorStateList.valueOf(Color.RED));
                holder.title.setText(item.title);
            } else if (item.title.equals("Maths")) {
                holder.title.setTextColor(ColorStateList.valueOf(Color.GREEN));
                holder.title.setText(item.title);
            } else if (item.title.equals("English")) {
                holder.title.setTextColor(ColorStateList.valueOf(Color.MAGENTA));
                holder.title.setText(item.title);

            } else if (item.title.equals("Hindi")) {
                holder.title.setTextColor(ColorStateList.valueOf(Color.DKGRAY));
                holder.title.setText(item.title);

            } else {
                holder.title.setText(item.title);
            }

            holder.hint.setText(item.hint);
            holder.hint1.setText(item.hint1);

            return convertView;
        }

        @Override
        public int getRealChildrenCount(int groupPosition) {
            return items.get(groupPosition).items.size();
        }

        @Override
        public GroupItem getGroup(int groupPosition) {
            return items.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return items.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            GroupHolder holder;
            GroupItem item = getGroup(groupPosition);
            if (convertView == null) {
                holder = new GroupHolder();
                convertView = inflater.inflate(R.layout.group_item, parent, false);
                holder.title = (TextView) convertView.findViewById(R.id.textTitle);
                Typeface font6 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
                holder.title.setTypeface(font6);
                convertView.setTag(holder);
            } else {
                holder = (GroupHolder) convertView.getTag();
            }

            holder.title.setText(item.title);

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int arg0, int arg1) {
            return true;
        }
    }


}

