package app.com.shiksha.activity;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import app.com.shiksha.R;
import app.com.shiksha.helper.CalendarView;
import app.com.shiksha.helper.UrlSupport;

public class NewAttendanceActivity extends AppCompatActivity {
    int month = 0, year = 0;
    String student_id;
    String attendance;
    private Toolbar toolbar;
    private ProgressDialog progressDialog;
    private Calendar cal = Calendar.getInstance();
    private ArrayList<Date> events = new ArrayList<>();
    private ArrayList<String> eventType = new ArrayList<>();
    private CalendarView cv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_attendance);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Working.....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        toolbar = (Toolbar) findViewById(R.id.toolbar_attendance);
//        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.navigation_icon);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        student_id = getIntent().getStringExtra("student_id");
        attendance = getIntent().getStringExtra("attentancevaalue");

        month = 1 + cal.get(Calendar.MONTH);
        year = cal.get(Calendar.YEAR);
        cv = (CalendarView) findViewById(R.id.calender_view);
        cv.setEventHandler(new CalendarView.EventHandler() {
            @Override
            public void onPreviousButtonClick() {
                if (month > 1) month--;
                else {
                    month = 12;
                    year--;
                }
                eventType.clear();
                events.clear();
                AttendanceApiData();
            }

            @Override
            public void onNextButtonClick() {
                if (month < 12) month++;
                else {
                    month = 1;
                    year++;
                }
                events.clear();
                eventType.clear();
                AttendanceApiData();

            }

            @Override
            public void onDayLongPress(Date date) {

            }

            @Override
            public void onDayPressed(Date date) {

            }
        });

        TextView textView = (TextView) findViewById(R.id.aggregate_text);
        Typeface font6 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textView.setTypeface(font6);
        TextView textView1 = (TextView) findViewById(R.id.text_percn);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textView1.setTypeface(font);
        textView1.setText(attendance);
        TextView textView2 = (TextView) findViewById(R.id.holiday);
        Typeface font1 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textView2.setTypeface(font1);
        TextView textView3 = (TextView) findViewById(R.id.absent);
        Typeface font2 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textView3.setTypeface(font2);
        TextView textView4 = (TextView) findViewById(R.id.present);
        Typeface font3 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textView4.setTypeface(font3);

        AttendanceApiData();

    }

    private void AttendanceApiData() {

        String mmonth;
        if (month < 10) {
            mmonth = "0" + month;
        } else {
            mmonth = "" + month;
        }
        String date = String.valueOf(year);
        Log.e("mmonth", date);

        Log.e("attendanceurl", UrlSupport.Attendance_api +"StudentId="+student_id+"&month="+mmonth+"&Year="+year);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, UrlSupport.Attendance_api +"StudentId="+student_id+"&month="+mmonth+"&Year="+year ,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("AttendanceResponse", "" + response);
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String sucess = jsonObject.getString("Status");
                            if (sucess.equals("Success")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                Log.e("arre", jsonArray.toString());
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
//                                    String student_id = jsonObject1.getString("student_id");
//                                    String section_id = jsonObject1.getString("section_id");
                                    String date = jsonObject1.getString("AttendanceDate");
                                    String status = jsonObject1.getString("Status");
                                    Log.e("date",date+""+status);

//                                    String created_at = jsonObject1.getString("created_at");
//                                    String class_name = jsonObject1.getString("class_name");
                                    String[] items = date.split("/");
                                    String yy = items[0];
                                    String mm = items[1];
                                    String dd = items[2];
                                    Log.e("divideddate1", "" + yy);
                                    Log.e("divideddate", "" + mm);
                                    cal.set(Calendar.YEAR, year);
                                    cal.set(Calendar.MONTH, month - 1);
                                    cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(yy));

                                    events.add(i, cal.getTime());
                                    eventType.add(i, status);
                                }

                            } else if (sucess.equals("Error")){
                             String msg = jsonObject.getString("Message");
                                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        cv.updateCalendar(events, eventType);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Something Went Wrong Please Try Again", Toast.LENGTH_LONG).show();

                    }
                }) {
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));



        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);

    }


}

