package app.com.shiksha.activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.adapter.HolidayListAdapter;
import app.com.shiksha.helper.ConnectivityReceiver;
import app.com.shiksha.helper.UrlSupport;
import app.com.shiksha.model.Holiday_List;

public class CalenderActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    boolean isConnected;
    private TextView list_of_holidays_text;
    private RecyclerView recyclerView;
    private List<Holiday_List> holiday_data;
    private HolidayListAdapter holidayListAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender);
        list_of_holidays_text = (TextView) findViewById(R.id.list_of_holidays_text);


        isConnected = ConnectivityReceiver.isConnected();
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        list_of_holidays_text.setTypeface(font);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_calender);
        toolbar.setNavigationIcon(R.drawable.navigation_icon);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (isConnected) {
            recyclerView = (RecyclerView) findViewById(R.id.holiday_list);
            recyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(layoutManager);
            HolidayListApi();
        }


    }

    private void HolidayListApi() {


        StringRequest stringrequest = new StringRequest(Request.Method.GET, UrlSupport.HolidayListApi,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("HolidayResponse", response);

                        holiday_data = new ArrayList<>();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("Status");
                            if (success.equals("Success")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    Holiday_List holiday = new Holiday_List();
                                    JSONObject jsonObj = jsonArray.getJSONObject(i);
                                    String name = jsonObj.getString("HolidayName");
                                    String date = jsonObj.getString("HolidayDate");
                                    holiday.setDate(date);
                                    holiday.setName(name);
                                    holiday_data.add(holiday);


                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        holidayListAdapter = new HolidayListAdapter(getApplicationContext(), holiday_data);
                        recyclerView.setAdapter(holidayListAdapter);
                        holidayListAdapter.notifyDataSetChanged();


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringrequest);
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}
