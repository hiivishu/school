package app.com.shiksha.activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.adapter.ClassMateAdapter;
import app.com.shiksha.helper.UrlSupport;
import app.com.shiksha.model.ClassMate;

public class ShopActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private RecyclerView classmateRV;
    private ClassMateAdapter classMateAdapter;
    private List<ClassMate> classMates_data;
    private String student_id,ClassId,ClassName,Section;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);
        student_id = getIntent().getStringExtra("student_id");
        ClassId = getIntent().getStringExtra("ClassId");
        ClassName = getIntent().getStringExtra("ClassName");
        Section = getIntent().getStringExtra("Section");

        toolbar = (Toolbar)findViewById(R.id.toolbar_shop);
        toolbar.setNavigationIcon(R.drawable.navigation_icon);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        classmateRV = (RecyclerView)findViewById(R.id.classmateRV);
        classmateRV.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        classmateRV.setLayoutManager(layoutManager);
        progressDialog = new ProgressDialog(ShopActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Working.....");
        progressDialog.setCanceledOnTouchOutside(false);


        ClassMateApi();

    }

    private void ClassMateApi() {

        progressDialog.show();
        StringRequest stringrequest = new StringRequest(Request.Method.GET, UrlSupport.ClassMateAPi+"StudentId="+student_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("ClassMateResponse", response);
                        progressDialog.dismiss();

                        classMates_data = new ArrayList<>();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject jsonObject1 = jsonObject.getJSONObject("Classmate");
                            JSONArray jsonArray = jsonObject1.getJSONArray("Data");
                            for (int i = 0; i<jsonArray.length();i++){
                                ClassMate classMate = new ClassMate();
                                JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                String StudentId = jsonObject2.getString("StudentId");
                                String StudentName = jsonObject2.getString("StudentName");
                                String DOB = jsonObject2.getString("DOB");
                                String ImagePath = jsonObject2.getString("ImagePath");
                                classMate.setDOB(DOB);
                                classMate.setImagePath(ImagePath);
                                classMate.setStudentId(StudentId);
                                classMate.setStudentName(StudentName);
                                classMates_data.add(classMate);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        classMateAdapter = new ClassMateAdapter(getApplicationContext(), classMates_data);
                        classmateRV.setAdapter(classMateAdapter);
                        classMateAdapter.notifyDataSetChanged();


                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();


            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringrequest);
    }
}
