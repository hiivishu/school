package app.com.shiksha.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mukesh.OtpView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import app.com.shiksha.MainActivity;
import app.com.shiksha.R;
import app.com.shiksha.helper.MyUtils;
import app.com.shiksha.helper.UrlSupport;


public class OtpVerificationActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences = null;
    SharedPreferences.Editor editor;
    String email, prime_phone;
    private String otp;
    private TextView resendotp,cancelotp;
    private ProgressDialog progressDialog;
    private OtpView otpview;
    private TextView submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);
        sharedPreferences = getSharedPreferences("Login_pref", 0);
        editor = sharedPreferences.edit();
        email = getIntent().getExtras().getString("email");
        prime_phone = getIntent().getExtras().getString("prime_phone");
        resendotp = (TextView) findViewById(R.id.resendcode);
        otpview = (OtpView) findViewById(R.id.otp_view);
        submit = (TextView) findViewById(R.id.submit);
        cancelotp = (TextView)findViewById(R.id.cancel);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                otp = otpview.getOTP().toString().trim();
                if (!otp.isEmpty() && MyUtils.hasActiveInternetConnection(getApplicationContext())) {

                    /*Verify Otp*/
                    editor.putInt("otpCancel",1);
                    editor.commit();
                    VerifyOtp();
                }else if (otp.isEmpty()){
                    Toast.makeText(getApplicationContext(),"OTP Can't be empty",Toast.LENGTH_LONG).show();
                }

                Log.e("otp", "" + otp);
//                VerifyOtp();
            }
        });
        resendotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ResendOtp();
            }
        });

        cancelotp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.putInt("otpCancel",0);
                editor.commit();
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
                finish();
            }
        });
    }


    private void VerifyOtp() {
        //Progress Dialog.
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Working.....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlSupport.VerifyOtp,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("LoginResponse", "" + response);

                        try {
                            JSONObject json = new JSONObject(response);

                            String sucess = json.getString("success");
                            String message = json.getString("message");

                            if (sucess.equals("true")){
                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(intent);
                            }else {
                                Toast.makeText(getApplicationContext(),message,Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Something Went Wrong", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("prime_phone", prime_phone);
                params.put("otp", otp);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);


    }

    public void ResendOtp() {

        //Progress Dialog.
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Working.....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlSupport.SendOtp,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("LoginResponse", "" + response);

                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Something Went Wrong", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                return params;
            }

        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);


    }
}
