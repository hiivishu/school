package app.com.shiksha.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import app.com.shiksha.R;

public class NoInternetconnectionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_internetconnection);
    }
}
