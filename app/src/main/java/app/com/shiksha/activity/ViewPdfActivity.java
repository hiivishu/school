package app.com.shiksha.activity;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import app.com.shiksha.R;

public class ViewPdfActivity extends AppCompatActivity {

    private WebView pdf_webView ;
    private final String GOOGLE_DOCS_URL = "http://drive.google.com/viewerng/viewer?embedded=true&url=";
    private ProgressDialog dialog;
    private String pdfURL;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pdf);

        pdf_webView = (WebView)findViewById(R.id.pdf_webView);

        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            pdfURL = bundle.getString("pdfUrl");
            Log.e("pdfUrl",pdfURL);
        }

        pdf_webView.getSettings().setLoadsImagesAutomatically(true);
        pdf_webView.getSettings().setJavaScriptEnabled(true);
        pdf_webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        pdf_webView.getSettings().setSupportZoom(true);
        pdf_webView.setWebViewClient(new Browser());
        pdf_webView.loadUrl(GOOGLE_DOCS_URL + pdfURL);
    }


    // Manages the behavior when URLs are loaded
    private class Browser extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            dialog.show();
        }

        @Override
        public void onPageFinished(WebView view, final String url) {
            dialog.dismiss();
        }

        @SuppressWarnings("deprecation")
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            view.loadUrl(request.getUrl().toString());
            return true;
        }
    }
}
