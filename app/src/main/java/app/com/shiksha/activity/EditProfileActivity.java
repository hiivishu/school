package app.com.shiksha.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import app.com.shiksha.R;
import app.com.shiksha.helper.ImageFilePath;
import app.com.shiksha.helper.UrlSupport;
import de.hdodenhof.circleimageview.CircleImageView;

public class EditProfileActivity extends AppCompatActivity {


    String name,email,pro_pic,phone_no,image;
    private CircleImageView circleImageView;
    private EditText edit_email,phone_no_edit,edit_name;
    private int PICK_IMAGE_REQUEST = 1;
    private Bitmap bitmap;
    private LinearLayout submit_edited_data;
    SharedPreferences sharedPreferences = null;
    SharedPreferences.Editor editor;
    int user_id;
    String gurdian_id;
    String pathimage;
    Uri yourUri;

    String selectedImagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        sharedPreferences = getSharedPreferences("Login_pref", 0);
        editor = sharedPreferences.edit();

        user_id = sharedPreferences.getInt("id", 0);

        gurdian_id = String.valueOf(user_id);
//        name = getIntent().getStringExtra("name");
        name = "Vishal";
//        email = getIntent().getStringExtra("email");
        email = "email@email.com";
        pro_pic = getIntent().getStringExtra("pro_pic");
//        phone_no = getIntent().getStringExtra("phone_no");
        phone_no = "9876543210";
        final ImageView imageView = (ImageView)findViewById(R.id.close_button);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        circleImageView = (CircleImageView)findViewById(R.id.gurdian_pic_edit);
        Picasso.with(getApplicationContext())
                .load(R.drawable.default_avatar)
                .placeholder(R.drawable.default_avatar) //this is optional the image to display while the url image is downloading
                .into(circleImageView);
        edit_email = (EditText)findViewById(R.id.edit_email);
        edit_email.setText(email);
        phone_no_edit = (EditText)findViewById(R.id.phone_no_edit);
        phone_no_edit.setText(phone_no);
        edit_name = (EditText)findViewById(R.id.edit_name);
        edit_name.setText(name);
        ImageView imageView1 = (ImageView)findViewById(R.id.edit_pic_edit);
        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });
        submit_edited_data = (LinearLayout)findViewById(R.id.submit_edited_data);
        submit_edited_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProfile();
//                image = getStringImage(bitmap);

                try {
                    Log.e("path",selectedImagePath);
                }catch (Exception e){

                }

                //Getting Image Name
                email = edit_email.getText().toString().trim();

                name = edit_name.getText().toString().trim();

                phone_no = phone_no_edit.getText().toString().trim();
            }
        });

    }

    private void updateProfile() {
        Log.e("de",name+""+email);
        //Showing the progress dialog
        final ProgressDialog loading = ProgressDialog.show(this,"Uploading...","Please wait...",false,false);
        loading.setCanceledOnTouchOutside(false);
        Log.e("updateprofileurl",UrlSupport.UpdateProfileApi+gurdian_id);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlSupport.UpdateProfileApi+gurdian_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        loading.dismiss();
                        //Disimissing the progress dialog
                        Log.e("updateResponse",s);

                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            JSONObject jsonObject1 = jsonObject.getJSONObject("result");
                            JSONObject jsonObject2 = jsonObject1.getJSONObject("guardian");
                            String name = jsonObject2.getString("name");
                            String email = jsonObject2.getString("email");
                            String prime_phone = jsonObject2.getString("prime_phone");
                            String profileimage_url = jsonObject2.getString("profileimage_url");
                            editor.putString("name", name);
                            editor.putString("email", email);
                            editor.putString("prime_phone", prime_phone);
                            editor.apply();

                            Toast.makeText(getApplicationContext(),"Profile Updated",Toast.LENGTH_LONG).show();
//                            finish();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //Showing toast message of the response

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        loading.dismiss();


                    }
                }){
            @Override
            protected Map<String, String> getParams()  {

                //Creating parameters
                Map<String,String> params = new HashMap<String,String>();

                //Adding parameters

                try {
                    if (selectedImagePath.equals("null")){

                    }else {
                        params.put("image", selectedImagePath);
                    }
                }catch (Exception e){

                }

                params.put("name", name);
                params.put("email",email);
                params.put("prime_phone",phone_no);
                params.put("sec_phone",phone_no);
                params.put("address","New Ashok Nagar");
                params.put("city","New Delhi");
                params.put("state","Delhi");

                //returning parameters
                return checkParams(params);
            }
        };


        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);

    }

    private Map<String,String> checkParams(Map<String, String> params) {
        Iterator<Map.Entry<String, String>> it = params.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> pairs = (Map.Entry<String, String>)it.next();
            if(pairs.getValue()==null){
                params.put(pairs.getKey(), "");
            }
        }
        return params;
    }

//    public String getStringImage(Bitmap bmp){
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
//        byte[] imageBytes = baos.toByteArray();
//        String PATH = Environment.getExternalStorageDirectory().getPath();
//        File f = new File(PATH);
//        yourUri = Uri.fromFile(f);
//        return PATH;
//    }

    private void showFileChooser() {
        if (Build.VERSION.SDK_INT <19){
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
        }

//        Intent intent = new Intent();
//        intent.setType("image/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();

            try {

                selectedImagePath = ImageFilePath.getPath(getApplicationContext(), filePath);
                Log.i("Image File Path", ""+selectedImagePath);
//                pathimage = getImagePath(filePath);
//                //Getting the Bitmap from Gallery
//                Log.e("im",pathimage);
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                //Setting the Bitmap to ImageView
                circleImageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

//    private String getImagePath(Uri filePath) {
//        Cursor cursor = getContentResolver().query(filePath,null,null,null,null);
//        cursor.moveToFirst();
//        String document_id = cursor.getString(0);
//        document_id = document_id.substring(document_id.lastIndexOf(":")+1);
//        cursor.close();
//        cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//                null , MediaStore.Images.Media._ID +" =  ? " , new String[]{document_id},null );
//        cursor.moveToFirst();
//        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
//        cursor.close();
//        return path;
//    }
}
