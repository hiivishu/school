package app.com.shiksha.activity;

import android.database.DataSetObserver;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import app.com.shiksha.R;
import app.com.shiksha.adapter.ChatArrayAdapter;
import app.com.shiksha.helper.ChatMessage;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatRoomActivity1 extends AppCompatActivity {
    private Toolbar toolbar;
    private ChatArrayAdapter chatArrayAdapter;
    private ListView listView;
    private TextView textsend;
    private EditText chatText;
    private boolean side = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room1);

        toolbar = (Toolbar) findViewById(R.id.toolbar_chatroom1);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        textsend = (TextView)findViewById(R.id.submit_message);
        listView = (ListView)findViewById(R.id.msgview);
        chatArrayAdapter = new ChatArrayAdapter(getApplicationContext(),R.layout.right);
        listView.setAdapter(chatArrayAdapter);
        chatText = (EditText)findViewById(R.id.message_data_box);
        chatText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)){
                    return sendChatMessage();
                }
                return false;
            }
        });
        textsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View args0) {
                sendChatMessage();
            }
        });

        listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listView.setAdapter(chatArrayAdapter);

        chatArrayAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                listView.setSelection(chatArrayAdapter.getCount() - 1);
            }
        });
        CircleImageView circleImageView = (CircleImageView)findViewById(R.id.principle_pic_inside);
        Picasso.with(getApplicationContext())
                .load(R.drawable.principle)
                .placeholder(R.drawable.default_avatar) //this is optional the image to display while the url image is downloading
                .into(circleImageView);
        TextView textView = (TextView)findViewById(R.id.chat1);
        TextView textView1 =(TextView)findViewById(R.id.chat2);
        TextView textView2 = (TextView)findViewById(R.id.chat3);
        TextView textView3 = (TextView)findViewById(R.id.chat4);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        textView.setTypeface(font);
        Typeface font1 = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        textView1.setTypeface(font1);
        Typeface font2 = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        textView2.setTypeface(font2);
        Typeface font3 = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        textView3.setTypeface(font3);

    }

    private boolean sendChatMessage() {
        chatArrayAdapter.add(new ChatMessage(side,chatText.getText().toString()));
        chatText.setText("");
        side = !side;
        return true;
    }

//
//        btn_send_msg = (TextView)findViewById(R.id.submit_message);
//        input_msg = (EditText)findViewById(R.id.message_data_box);
//        chat_conversation = (TextView)findViewById(R.id.chat_text);
//
//        user_name = getIntent().getExtras().get("user_name").toString();
//        room_name = getIntent().getExtras().get("room_name").toString();
//        setTitle(room_name);
//
//        root = FirebaseDatabase.getInstance().getReference().child(room_name);
//        btn_send_msg.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Map<String,Object> map = new HashMap<String, Object>();
//                temp_key = root.push().getKey();
//                root.updateChildren(map);
//
//                DatabaseReference message_root = root.child(temp_key);
//                Map<String,Object> map2 = new HashMap<String, Object>();
//                map2.put("name",user_name);
//                map2.put("msg",input_msg.getText().toString());
//
//                message_root.updateChildren(map2);
//            }
//        });
//
//        root.addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                append_chat_conversation(dataSnapshot);
//
//            }
//
//            @Override
//            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                append_chat_conversation(dataSnapshot);
//            }
//
//            @Override
//            public void onChildRemoved(DataSnapshot dataSnapshot) {
//
//            }
//
//            @Override
//            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });
//
//
//
//    }
//private String chat_msg,chat_user_name;
//    private void append_chat_conversation(DataSnapshot dataSnapshot) {
//        Iterator i = dataSnapshot.getChildren().iterator();
//        while (i.hasNext()){
//
//            chat_msg = (String) ((DataSnapshot)i.next()).getValue();
//            chat_user_name =  (String)((DataSnapshot)i.next()).getValue();
//
//            chat_conversation.append(chat_user_name+":"+chat_msg+"\n");
//        }

    }
