package app.com.shiksha.activity;

import android.app.DownloadManager;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.com.shiksha.R;
import app.com.shiksha.helper.ConnectivityReceiver;
import app.com.shiksha.helper.UrlSupport;

public class StudyMaterialActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, ConnectivityReceiver.ConnectivityReceiverListener {
    String student_id;
    boolean isConnected;
    private Toolbar toolbar;
    private JSONArray result;
    private ArrayList<String> sub_details;
    private Spinner spinner;
    private TextView textView, textView1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_study_material);
        student_id = getIntent().getStringExtra("student_id");
        isConnected = ConnectivityReceiver.isConnected();
        toolbar = (Toolbar) findViewById(R.id.toolbar_study_material);

        toolbar.setNavigationIcon(R.drawable.navigation_icon);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        sub_details = new ArrayList<>();

        spinner = (Spinner) findViewById(R.id.spinner_study_material);

        // Spinner click listener
        spinner.setOnItemSelectedListener(this);

        textView = (TextView) findViewById(R.id.download_data_img);
        textView1 = (TextView) findViewById(R.id.file_name_text);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        textView1.setTypeface(font);


        if (isConnected) {
            getStudyMaterialData();
        }


    }

    private void getStudyMaterialData() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, UrlSupport.StudyMaterialApi + student_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("StudyMaterial", "" + response);

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String success = jsonObject.getString("success");
                            if (success.equals("true")) {
                                result = jsonObject.getJSONArray("result");

                                getDetails(result);


                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), "Something Went Wrong Please Try Again", Toast.LENGTH_LONG).show();

                    }
                }) {
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);

    }

    private void getDetails(JSONArray result) {
        for (int i = 0; i < result.length(); i++) {

            try {
                JSONObject json = result.getJSONObject(i);

                String subject = json.getString("subject");
                sub_details.add(subject);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        spinner.setAdapter(new ArrayAdapter<String>(getApplicationContext(), R.layout.spinnertextstudymaterial, sub_details));

    }

    private String getstudy_material_file_url(int postion) {
        String study_material_file_url = "";
        try {
            JSONObject jsonObject = result.getJSONObject(postion);
            study_material_file_url = jsonObject.getString("study_material_file_url");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return study_material_file_url;
    }

    private String getFile_Name(int position) {
        String filename = " ";
        try {
            JSONObject jsonObject = result.getJSONObject(position);
            filename = jsonObject.getString("filename");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return filename;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, final int position, long id) {

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isConnected) {

                    //download file
                    Toast.makeText(getApplicationContext(), "Downloading started", Toast.LENGTH_SHORT).show();
                    DownloadManager.Request request = new DownloadManager.Request(Uri.parse(getstudy_material_file_url(position)));
                    request.setDescription("Downloading Study Material");
                    request.setTitle(getFile_Name(position));
                    // in order for this if to run, you must use the android 3.2 to compile your app
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        request.allowScanningByMediaScanner();
                        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                    }
                    request.setDestinationInExternalPublicDir("Schulerz/", getFile_Name(position));

                    // get download service and enqueue file
                    DownloadManager manager = (DownloadManager) getApplicationContext().getSystemService(Context.DOWNLOAD_SERVICE);
                    manager.enqueue(request);
                    Toast.makeText(getApplicationContext(), "Downloading started", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), R.string.no_internet_warning, Toast.LENGTH_LONG).show();
                }
            }
        });

        textView1.setText(getFile_Name(position));

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {


    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}
