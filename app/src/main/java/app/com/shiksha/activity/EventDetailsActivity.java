package app.com.shiksha.activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import app.com.shiksha.R;
import de.hdodenhof.circleimageview.CircleImageView;

public class EventDetailsActivity extends AppCompatActivity {
    String img, going_heading, going_date, going_venue,description,going_time,going_pepole_url;
    int noofgoing;
    CircleImageView circleImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);

        img = getIntent().getStringExtra("going_img");
        going_heading = getIntent().getStringExtra("going_heading");
        going_date = getIntent().getStringExtra("going_date");
        going_venue = getIntent().getStringExtra("going_venue");
        noofgoing = getIntent().getIntExtra("noofgoing",0);
        description = getIntent().getStringExtra("description");
        going_time = getIntent().getStringExtra("going_time");
        going_pepole_url = getIntent().getStringExtra("peopleimg");


        ImageView imageView = (ImageView) findViewById(R.id.deimg);
        CircleImageView circleImageView = (CircleImageView)findViewById(R.id.student_pic_ev_detail);


        Picasso.with(getApplicationContext())
                .load(going_pepole_url)
                .placeholder(R.color.simplegray) //this is optional the image to display while the url image is downloading
                .into(circleImageView);
        Picasso.with(getApplicationContext())
                .load(img)
                .placeholder(R.color.simplegray) //this is optional the image to display while the url image is downloading
                .into(imageView);


        TextView textView = (TextView)findViewById(R.id.event_detail_heading);
        textView.setText(String.valueOf(going_heading));
        Typeface font5 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textView.setTypeface(font5);
        TextView textView1 = (TextView)findViewById(R.id.date);
        textView1.setText(String.valueOf(going_date));
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textView1.setTypeface(font);
        TextView textView2 = (TextView)findViewById(R.id.venue_event);
        textView2.setText(String.valueOf(going_venue));
        Typeface font2 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textView2.setTypeface(font2);
        TextView textView3 = (TextView)findViewById(R.id.noofgoing_details);
        textView3.setText("+"+String.valueOf(noofgoing));
        Typeface font3 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textView3.setTypeface(font3);
        TextView textView4 = (TextView)findViewById(R.id.description);
        textView4.setText(String.valueOf(description));
        Typeface font4 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textView4.setTypeface(font4);
        TextView textView5 = (TextView)findViewById(R.id.going_time);
        textView5.setText(String.valueOf(going_time));
        Typeface font6 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textView5.setTypeface(font6);
        TextView textView6 = (TextView)findViewById(R.id.areattendingtext_details);
        Typeface font7 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textView6.setTypeface(font7);
        TextView textView7 = (TextView)findViewById(R.id.detailstext);
        Typeface font8 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textView7.setTypeface(font8);

        ImageView imageView1 = (ImageView)findViewById(R.id.back_icon);
        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
