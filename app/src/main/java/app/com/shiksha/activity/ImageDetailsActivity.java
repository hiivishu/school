package app.com.shiksha.activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.adapter.GalleryDetailAdapter;
import app.com.shiksha.helper.ConnectivityReceiver;
import app.com.shiksha.model.GalleryDetail;

public class ImageDetailsActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener{
    boolean isConnected;
    private RecyclerView gallery_detail_rv;
    private List<GalleryDetail>   galleryDetails_data;
    private GalleryDetailAdapter galleryDetailAdapter;
    private ProgressDialog progressDialog;
//    int imageId;
    String Title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_details);
        ImageView finishActivity = (ImageView)findViewById(R.id.finishActivity);
        finishActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
//        imageId =getIntent().getIntExtra("img_id",0);
//        Log.e("imageId",""+imageId);
        Title = getIntent().getStringExtra("Title").replace(" ","%20");
        isConnected = ConnectivityReceiver.isConnected();
        if (isConnected){
            galleryDetailApi(Title);
            gallery_detail_rv = (RecyclerView)findViewById(R.id.gallery_detail_rv);
            gallery_detail_rv.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ImageDetailsActivity.this);
            gallery_detail_rv.setLayoutManager(layoutManager);
        }
    }

    private void galleryDetailApi(String Title) {

        progressDialog = new ProgressDialog(ImageDetailsActivity.this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Working.....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        Log.e("ADDD",Title);
        String url = "http://ccclucknow.com/ccclucknow/WebService.asmx/GetGalleryByAlbumName?AlbumName="+Title;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("GalleryDetailResponse", response);

                        galleryDetails_data = new ArrayList<>();

                        try {
                            JSONObject jsonObject =  new JSONObject(response);
                            JSONObject jsonObject1 = jsonObject.getJSONObject("GalleryByAlbumName");

                            JSONArray jsonArray = jsonObject1.getJSONArray("Data");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                GalleryDetail galleryDetail = new GalleryDetail();
                                JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                String Image = jsonObject2.getString("ImagePath");
                                String gallery_image_url1 = Image.replaceAll("~", "");
                                galleryDetail.setImage(gallery_image_url1);
                                galleryDetails_data.add(galleryDetail);


                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        galleryDetailAdapter = new GalleryDetailAdapter(ImageDetailsActivity.this,galleryDetails_data);
                        gallery_detail_rv.setAdapter(galleryDetailAdapter);
                        galleryDetailAdapter.notifyDataSetChanged();
                        progressDialog.dismiss();


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    }
                }) {

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);




    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}
