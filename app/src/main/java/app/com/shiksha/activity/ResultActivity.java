package app.com.shiksha.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.adapter.ResultAdapter;
import app.com.shiksha.helper.ConnectivityReceiver;
import app.com.shiksha.helper.UrlSupport;
import app.com.shiksha.model.Result;

public class ResultActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    String student_id;
    boolean isConnected;
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private ResultAdapter reultadapter;
    private List<Result> result_data;
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        toolbar = (Toolbar) findViewById(R.id.toolbar_result);
        toolbar.setNavigationIcon(R.drawable.navigation_icon);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        student_id = getIntent().getStringExtra("student_id");
        isConnected = ConnectivityReceiver.isConnected();

//        TextView textview = (TextView) findViewById(R.id.grade_result_text);
//        Typeface font6 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
//        textview.setTypeface(font6);
//        TextView textview1 = (TextView) findViewById(R.id.grade_value);
//        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
//        textview1.setTypeface(font);
//        TextView textview2 = (TextView) findViewById(R.id.marks);
//        Typeface font3 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
//        textview2.setTypeface(font3);
//        TextView textview4 = (TextView) findViewById(R.id.marksobtain);
//        Typeface font5 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
//        textview4.setTypeface(font5);
//        TextView textview7 = (TextView) findViewById(R.id.totalmarks);
//        Typeface font7 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
//        textview7.setTypeface(font7);
//        TextView textview8 = (TextView) findViewById(R.id.totalmarksvalue);
//        Typeface font9 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
//        textview8.setTypeface(font9);
        if (isConnected) {

            //Get webview
            webView = (WebView) findViewById(R.id.webView1);

            startWebView("http://koshishindia.co.in/StudentResults/2nd-A-STU000001.html");

//            recyclerView = (RecyclerView) findViewById(R.id.result_recycle_lay);
//            recyclerView.setHasFixedSize(true);
//            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
//            recyclerView.setLayoutManager(layoutManager);
//            resultApi();
        }
    }


    private void startWebView(String url) {

        //Create new webview Client to show progress dialog
        //When opening a url or click on link

        webView.setWebViewClient(new WebViewClient() {
            ProgressDialog progressDialog;

            //If you will not use this method url links are opeen in new brower not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            //Show loader on url load
            public void onLoadResource (WebView view, String url) {
                if (progressDialog == null) {
                    // in standard case YourActivity.this
                    progressDialog = new ProgressDialog(ResultActivity.this);
                    progressDialog.setMessage("Loading...");
                    progressDialog.show();
                }
            }
            public void onPageFinished(WebView view, String url) {
                try{
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }
                }catch(Exception exception){
                    exception.printStackTrace();
                }
            }

        });

        // Javascript inabled on webview
        webView.getSettings().setJavaScriptEnabled(true);


        //Load url in webview
        webView.loadUrl(url);


    }

    @Override
    // Detect when the back button is pressed
    public void onBackPressed() {
        if(webView.canGoBack()) {
            webView.goBack();
        } else {
            // Let the system handle the back button
            super.onBackPressed();
        }
    }
    private void resultApi() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, UrlSupport.ResultApi+"2",
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("ResultResponse", "" + response);

                        result_data = new ArrayList<>();
                        try {
                            JSONObject jsonobject = new JSONObject(response);

                            String success = jsonobject.getString("success");
                            if (success.equals("true")) {

                                int marks_obtained = jsonobject.getInt("marks_obtained");
                                int total_marks = jsonobject.getInt("total_marks");
                                String total_grade = jsonobject.getString("total_grade");

                                JSONArray jsonarray = jsonobject.getJSONArray("result");
                                for (int i = 0; i < jsonarray.length(); i++) {

                                    Result resultdata = new Result();
                                    JSONObject jsonobject1 = jsonarray.getJSONObject(i);
                                    int marks_obtained_detail = jsonobject1.getInt("marks_obtained");
                                    String grade = jsonobject1.getString("grade");
                                    String subject_name = jsonobject1.getString("subject_name");
                                    String exam_name = jsonobject1.getString("exam_name");
                                    int max_marks = jsonobject1.getInt("max_marks");


                                    resultdata.setExam_name(exam_name);
                                    resultdata.setGrade(grade);
                                    resultdata.setMarks_obtained(marks_obtained_detail);
                                    resultdata.setSubject_name(subject_name);
                                    resultdata.setMax_marks(max_marks);

                                    result_data.add(resultdata);

                                }

                                setresultvalue(marks_obtained, total_marks,total_grade);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        reultadapter = new ResultAdapter(getApplicationContext(), result_data);
                        recyclerView.setAdapter(reultadapter);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);


    }

    private void setresultvalue(int marks_obtained, int total_marks,String total_grade) {
        TextView textview = (TextView) findViewById(R.id.marksobtain);
        textview.setText(String.valueOf(marks_obtained));
        TextView textview1 = (TextView) findViewById(R.id.totalmarksvalue);
        textview1.setText(String.valueOf(total_marks));
        TextView textView = (TextView)findViewById(R.id.grade_value);
        textView.setText(String.valueOf(total_grade));

    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}
