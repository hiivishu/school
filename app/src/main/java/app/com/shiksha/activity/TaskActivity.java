package app.com.shiksha.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import app.com.shiksha.R;
import app.com.shiksha.adapter.HomeWorkAdapter;
import app.com.shiksha.helper.ConnectivityReceiver;
import app.com.shiksha.helper.UrlSupport;
import app.com.shiksha.model.HomeWork;



public class TaskActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    String student_id,ClassId,ClassName,Section;
    boolean isConnected;
    HomeWork homeWork;
    int id, submission_status, subject_id;
    private Toolbar toolbar;
    private TextView textView;
    private RecyclerView recyclerView;
    private HomeWorkAdapter homeWorkAdapter;
    private ArrayList<HomeWork> homework_data;
    private ProgressDialog progressDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Working.....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        toolbar = (Toolbar) findViewById(R.id.toolbar_task);
        toolbar.setNavigationIcon(R.drawable.navigation_icon);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
//        textView = (TextView) findViewById(R.id.chemistry_details);
//        textView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                FragmentManager fragmentManager = getSupportFragmentManager();
//                TaskChemistryFragment taskChemistryFragment = new TaskChemistryFragment();
//                taskChemistryFragment.show(fragmentManager, "abc");
//            }
//        });
        isConnected = ConnectivityReceiver.isConnected();
        student_id = getIntent().getStringExtra("student_id");
        ClassId = getIntent().getStringExtra("ClassId");
        ClassName = getIntent().getStringExtra("ClassName");
        Section = getIntent().getStringExtra("Section");




    }

    @Override
    protected void onStart() {
        super.onStart();
        if (isConnected) {
            recyclerView = (RecyclerView) findViewById(R.id.homework_recycle_lay);
            recyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(layoutManager);
            HomeWorkApi();
        }
    }

    private void HomeWorkApi() {
        Log.e("homeworkapi",UrlSupport.HomeWorkApi+"ClassId="+"1"+"&Section="+"A");
        StringRequest stringRequest = new StringRequest(Request.Method.GET, UrlSupport.HomeWorkApi+"ClassId="+ClassId+"&Section="+Section,

                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("HomeworkResponse", "" + response);

                        homework_data = new ArrayList<>();


                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject jsonObject1 = jsonObject.getJSONObject("Assignment");


                                JSONArray jsonArray = jsonObject1.getJSONArray("Data");
                                if (jsonArray.length()==0){
                                 Toast.makeText(getApplicationContext(),"No HomeWork Found ",Toast.LENGTH_LONG).show();
                                }else {


                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        HomeWork homeWork = new HomeWork();
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        String Title = jsonObject2.getString("AssignmenrTitle");
                                        String Subject = jsonObject2.getString("Subject");
                                        String HomeworkDescription = jsonObject2.getString("FileUpload");
                                        homeWork.setTitle(Title);
                                        homeWork.setSubject(Subject);
                                        homeWork.setHomework_status(HomeworkDescription);
                                        homework_data.add(homeWork);

                                    }
                                }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        homeWorkAdapter = new HomeWorkAdapter(getApplicationContext(), homework_data);
                        recyclerView.setAdapter(homeWorkAdapter);
                        progressDialog.dismiss();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Toast.makeText(getApplicationContext(),"Something Went Wrong",Toast.LENGTH_LONG).show();
                    }
                }) {
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);

    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }
}
