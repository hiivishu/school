package app.com.shiksha.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import app.com.shiksha.R;
import app.com.shiksha.helper.UrlSupport;

public class ReportProblemActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private Bitmap bitmap;
    ImageView imageView;
    SharedPreferences sharedPreferences = null;
    SharedPreferences.Editor editor;
    int user_id;
    String gurdian_id;
    private String image,name,description ;

    private EditText editTextName ,editTextNamedes;
    private Uri filePath;

    private int PICK_IMAGE_REQUEST = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_problem);

        sharedPreferences = getSharedPreferences("Login_pref", 0);
        editor = sharedPreferences.edit();

        user_id = sharedPreferences.getInt("id", 0);

        gurdian_id = String.valueOf(user_id);
        toolbar = (Toolbar)findViewById(R.id.toolbar_report_problem);
        toolbar.setNavigationIcon(R.drawable.navigation_icon);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        imageView  = (ImageView) findViewById(R.id.selected_image);
        editTextName = (EditText)findViewById(R.id.title_data);
        editTextNamedes = (EditText)findViewById(R.id.description_data);


        Button button = (Button)findViewById(R.id.choose_file);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFileChooser();
            }
        });
        Button button1 = (Button)findViewById(R.id.submit_report_data);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Getting Image Name
                name = editTextName.getText().toString().trim();

                description = editTextNamedes.getText().toString().trim();
                if (name.isEmpty()&&description.isEmpty()){
                    Toast.makeText(getApplicationContext(),"Fill the Details",Toast.LENGTH_LONG).show();
                }else {
                    UploadData();
                }


            }
        });
    }

    private void UploadData() {

        //Showing the progress dialog
        final ProgressDialog loading = ProgressDialog.show(this,"Uploading...","Please wait...",false,false);
        loading.setCanceledOnTouchOutside(false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UrlSupport.ReportAProblem+gurdian_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        //Disimissing the progress dialog
                        loading.dismiss();
                        //Showing toast message of the response
                        try {
                            JSONObject jsonObject = new JSONObject(s);
                            String success = jsonObject.getString("success");
                            if (success.equals("true")){
                                Toast.makeText(getApplicationContext(),"Problem Reported",Toast.LENGTH_LONG).show();
                                editTextName.setText("");
                                editTextNamedes.setText("");
                                imageView.setImageBitmap(null);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.e("re",s);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        loading.dismiss();

                        if (volleyError.networkResponse == null) {
                            if (volleyError.getClass().equals(TimeoutError.class)) {
                                // Show timeout error message
                                Toast.makeText(getApplicationContext(),
                                        "Oops. Something Went Wrong!",
                                        Toast.LENGTH_LONG).show();
                            }
                        }
                            Toast.makeText(ReportProblemActivity.this, "Make Sure You Selected ScreenShot.", Toast.LENGTH_LONG).show();

                        //Showing toast

                    }
                }){
            @Override
            protected Map<String, String> getParams()  {
                //Converting Bitmap to String

                image = getStringImage(bitmap);



                Log.e("path",name+""+description);

                //Creating parameters
                Map<String,String> params = new HashMap<>();

                //Adding parameters
                params.put("image", image);
                params.put("title", name);
                params.put("description",description);

                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    private void showFileChooser() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                //Getting the Bitmap from Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                //Setting the Bitmap to ImageView
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
