package app.com.shiksha.activity;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import app.com.shiksha.R;
import app.com.shiksha.adapter.EventsAdapter;
import app.com.shiksha.adapter.EventsAdapterGoing;
import app.com.shiksha.helper.ConnectivityReceiver;
import app.com.shiksha.helper.UrlSupport;
import app.com.shiksha.model.NewEventsgoing;

public class EventsActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    boolean isConnected;
    String gurdian_id;
    private Toolbar toolbar;
    private RecyclerView recyclerView, recyclerView1;
    private EventsAdapter eventadapter;
    private EventsAdapterGoing eventsAdapterGoing;
    private List<NewEventsgoing> event_going;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout linlaheaderprogress, no_internet_lay;
    private TextView no_invi;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
//Progress Dialog.
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Working.....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
        toolbar = (Toolbar) findViewById(R.id.toolbar_events);
        toolbar.setNavigationIcon(R.drawable.navigation_icon);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        gurdian_id = getIntent().getStringExtra("gurdian_id");

        isConnected = ConnectivityReceiver.isConnected();
        no_internet_lay = (LinearLayout) findViewById(R.id.no_Internet_lay_events);

        if (isConnected) {
            recyclerView = (RecyclerView) findViewById(R.id.notgoing);
            recyclerView1 = (RecyclerView) findViewById(R.id.going);
            no_invi = (TextView) findViewById(R.id.no_invi);
            recyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(layoutManager);

            EventsApi();

        } else {
            no_internet_lay.setVisibility(View.VISIBLE);
        }

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout_events);


        /**
         * Showing Swipe Refresh animation on activity create
         * As animation won't start on onCreate, post runnable is used
         */

        TextView textView = (TextView) findViewById(R.id.invitedtext);
        Typeface font5 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textView.setTypeface(font5);
        TextView textView1 = (TextView) findViewById(R.id.goingtext);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        textView1.setTypeface(font);
        Typeface font2 = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Regular.ttf");
        no_invi.setTypeface(font2);

    }

    private void EventsApi() {

        Log.e("urlEvents", UrlSupport.EventsApi);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, UrlSupport.EventsApi,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        Log.e("EventsResponse", "" + response);


                        event_going = new ArrayList<>();


                        try {

                            JSONObject jsonobject = new JSONObject(response);
                            JSONObject jsonObject = jsonobject.getJSONObject("Events");

                            JSONArray jsonArray = jsonObject.getJSONArray("Data");
                                for (int j = 0; j < jsonArray.length(); j++) {
                                    NewEventsgoing neweventgoing = new NewEventsgoing();
                                    JSONObject jsonobject3 = jsonArray.getJSONObject(j);
                                    String EventName = jsonobject3.getString("EventName");
                                    String EventDate = jsonobject3.getString("EventDate");
                                    String Description = jsonobject3.getString("Description");
                                    neweventgoing.setDescription(Description);
                                    neweventgoing.setEventDate(EventDate);
                                    neweventgoing.setEventName(EventName);
//                                    neweventgoing.setEventPlace(EventPlace);
//                                    neweventgoing.setEventTime(EventTime);
//                                    neweventgoing.setFileUpload(gallery_image_url1);
                                    event_going.add(neweventgoing);
                                }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        eventsAdapterGoing = new EventsAdapterGoing(getApplicationContext(), event_going);
                        recyclerView.setAdapter(eventsAdapterGoing);
                        progressDialog.dismiss();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        error.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Something Went Wrong", Toast.LENGTH_LONG).show();
                    }
                }) {
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
               3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));



        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        no_internet_lay.setVisibility(View.GONE);
    }

}
