package app.com.shiksha.activity;

import android.database.DataSetObserver;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import app.com.shiksha.R;
import app.com.shiksha.adapter.ChatArrayAdapter;
import app.com.shiksha.helper.ChatMessage;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatRoomActivity3 extends AppCompatActivity {
    private Toolbar toolbar;
    private ChatArrayAdapter chatArrayAdapter;
    private ListView listView;
    private TextView textsend;
    private EditText chatText;
    private boolean side = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room3);
        toolbar = (Toolbar) findViewById(R.id.toolbar_chatroom3);
        toolbar.setNavigationIcon(R.drawable.navigation_icon);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        textsend = (TextView)findViewById(R.id.submit_message2);
        listView = (ListView)findViewById(R.id.msgview2);
        chatArrayAdapter = new ChatArrayAdapter(getApplicationContext(),R.layout.right);
        listView.setAdapter(chatArrayAdapter);
        chatText = (EditText)findViewById(R.id.message_data_box2);
        chatText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)){
                    return sendChatMessage();
                }

                return false;
            }
        });
        textsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View args0) {
                sendChatMessage();
            }
        });

        listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listView.setAdapter(chatArrayAdapter);

        chatArrayAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                listView.setSelection(chatArrayAdapter.getCount() - 1);
            }
        });
        CircleImageView circleImageView1 = (CircleImageView)findViewById(R.id.school_admin);
        Picasso.with(getApplicationContext())
                .load(R.drawable.schooladmin)
                .placeholder(R.drawable.default_avatar) //this is optional the image to display while the url image is downloading
                .into(circleImageView1);
        TextView textView = (TextView)findViewById(R.id.chat31);
        TextView textView1 = (TextView)findViewById(R.id.chat32);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        textView.setTypeface(font);
        Typeface font1 = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        textView1.setTypeface(font1);
    }

    private boolean sendChatMessage() {
        chatArrayAdapter.add(new ChatMessage(side,chatText.getText().toString()));
        chatText.setText("");
        side = !side;
        return true;
    }
}
