package app.com.shiksha.activity;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import app.com.shiksha.R;
import app.com.shiksha.model.Chat_room_msg;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatRoomActivity extends AppCompatActivity {

    String person_name,person_image,room_name;
    private DatabaseReference root ;
    EditText editText;
    TextView textView;
    private TextView chat_conversation;
    private String temp_key;
    private String chat_msg,chat_user_name;
    SharedPreferences sharedPreferences = null;
    SharedPreferences.Editor editor;
    int user_id;
    private ArrayAdapter<String> arrayAdapter;
    String gurdian_id;
    String type;
    String name;
    ListView chat_list_view;
    String ch;
    private ArrayList<String> chat_data = new ArrayList<>() ;
//    private ChatMessageListAdapter chatMessageListAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room);
        sharedPreferences = getSharedPreferences("Login_pref", 0);
        editor = sharedPreferences.edit();
        user_id = sharedPreferences.getInt("id", 0);
        gurdian_id = String.valueOf(user_id);
        type = sharedPreferences.getString("type","");
        name = sharedPreferences.getString("name","");

        person_name = getIntent().getStringExtra("person_name");
        person_image = getIntent().getStringExtra("person_image");
        room_name = getIntent().getStringExtra("room_name");
        Log.e("room_name",room_name+" "+person_image+" "+person_name);

        editText = (EditText)findViewById(R.id.message_data_box_chat_room);
        textView = (TextView)findViewById(R.id.submit_message_chat_room);
        chat_conversation = (TextView) findViewById(R.id.textView_chat);
        chat_list_view = (ListView) findViewById(R.id.chat_list_view);
        chat_list_view.setStackFromBottom(true);
        chat_list_view.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);

        editText.setInputType(InputType.TYPE_CLASS_TEXT
                | InputType.TYPE_TEXT_FLAG_MULTI_LINE);



        root = FirebaseDatabase.getInstance().getReference().child(room_name);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText.getText().toString().isEmpty()){
                     //Do Nothing.
                }else {
                Map<String,Object> map = new HashMap<String, Object>();
                temp_key = root.push().getKey();
                root.updateChildren(map);

                DatabaseReference message_root = root.child(temp_key);
                Map<String,Object> map2 = new HashMap<String, Object>();
                map2.put("name",name);
                map2.put("msg",editText.getText().toString());

                message_root.updateChildren(map2);
                editText.setText("");
                }
            }
        });


        CircleImageView circleImageView = (CircleImageView)findViewById(R.id.chat_person_pic);
        Picasso.with(getApplicationContext())
                .load(person_image)
                .placeholder(R.color.simplegray) //this is optional the image to display while the url image is downloading
                .into(circleImageView);
        TextView textView = (TextView)findViewById(R.id.chat_person_name);
        textView.setText(person_name);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        textView.setTypeface(font);
        Typeface font1 = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        chat_conversation.setTypeface(font1);
        ImageView imageView = (ImageView)findViewById(R.id.back_icon_chat);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        root.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                append_chat_conversation(dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                append_chat_conversation(dataSnapshot);

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    public void append_chat_conversation(DataSnapshot dataSnapshot) {
        Set<String> set = new HashSet<String>();
        Iterator i = dataSnapshot.getChildren().iterator();


        while (i.hasNext()){

//           set.add(String.valueOf(((DataSnapshot)i.next()).getValue()));

            Chat_room_msg chat_room_msg = new Chat_room_msg();
            chat_msg = (String) ((DataSnapshot)i.next()).getValue();
            chat_user_name = (String) ((DataSnapshot)i.next()).getValue();
            ch = chat_user_name+" : "+chat_msg;
            chat_room_msg.setMsg(ch);
//            chat_data.add(chat_room_msg);
            set.add(ch);
//

//            chat_conversation.append(name +" : "+chat_msg +" \n"+" \n");
        }
        chat_data.addAll(set);
//        chatMessageListAdapter = new ChatMessageListAdapter(getApplicationContext(),chat_data);
//        recyclerView.setAdapter(chatMessageListAdapter);



        arrayAdapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.chat_single_design,R.id.chat_data_view,chat_data);
        chat_list_view.setAdapter(arrayAdapter);


        arrayAdapter.notifyDataSetChanged();


    }
}
